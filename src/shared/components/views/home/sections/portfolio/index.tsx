"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { Methodology } from "@timcowebapps/react.style";
import { Heading } from "@timcowebapps/react.toolkit";
import { HeadingVariants } from "./../../../../ui/typography/heading/variants";
import CallmeForm from "./../../../../partials/callme-form/index";
import { PortfolioProps } from "./index-props";
import { PortfolioState } from "./index-state";

//#region Import Assets

const styles = require("./../../index.scss");

//#endregion

export default class Portfolio extends React.Component<PortfolioProps.IProps, PortfolioState.IState> {
	//#region Статические переменные

	public static displayName: string = "Portfolio";
	public static propTypes: PropTypes.ValidationMap<PortfolioProps.IProps> = PortfolioProps.types;
	public static defaultProps: PortfolioProps.IProps = PortfolioProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Portfolio
	 * @private
	 */
	private _getInitialState(): PortfolioState.IState {
		return {
		}
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Portfolio
	 * @public
	 * @constructor
	 * @param {PortfolioProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: PortfolioProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Portfolio
	 * @public
	 */
	public render(): JSX.Element {
		return (
			<div className={styles["container"]}>
				{/* fieldable */}
				<div className={styles["row"]}>
					<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
						<Heading {...HeadingVariants.getSectionTitleSheme(styles, "Наши работы")} />
					</div>
				</div>
				
				<CallmeForm />
			</div>
		);
	}
}
