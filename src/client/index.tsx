"use strict";

import * as React from "react";
import * as ReactDOM from "react-dom";
import * as Loadable from "react-loadable";
import { createBrowserHistory } from "history";
import { configureStore } from "./../shared/configurestore";
import { FirebaseService } from "./../shared/services/firebaseservice";
import { Root } from "./root";

import "./pre-fonts.scss";
import "./prereqs.scss";

console.info(`Enabling devtools for "${process.env.NODE_ENV}" mode`);
console.info("Version:", process.env.VERSION);

const preloadedState = (window as any).__INITIAL_DATA__;
delete (window as any).__INITIAL_DATA__;

FirebaseService.instance;

const history = createBrowserHistory();
const root = document.getElementById("root");
const application = <Root store={configureStore(history, preloadedState)} routerHistory={history} />;

Loadable.preloadReady().then(() => {
	ReactDOM.hydrate(application, root);
});

if ((module as any).hot) {
	(module as any).hot.accept((err: any) => {
		if (err)
			console.error("Cannot apply HMR update.", err);
	});
}
