"use strict";

import { Action, ActionCreator } from "redux";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { AddToCartActionTypeState } from "./addtocartactiontypestate";

export const addToCartFailureAction: ActionCreator<Action> = (error: any): IAction<any> => {
	return {
		type: AddToCartActionTypeState.toStr(ActionTypeStates.FAILURE),
		payload: {
			error: error
		}
	};
}

export const addToCartFailure = addToCartFailureAction;
