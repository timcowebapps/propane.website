"use strict";

import { RouteComponentProps } from "react-router";
import * as PropTypes from "prop-types";
import { bindActionCreators, Dispatch } from "redux";
import { IAction } from "./../../../actions/action";
import { Actions } from "./../../../sagas/@exports";

export namespace ShopPageProps {
	export interface IOwnProps extends RouteComponentProps<any> {
		// Empty
	}

	export interface IStateProps {
		products: Array<any>;
	}

	export interface IDispatchProps {
		actionCreators: {
			receiveProducts?: () => IAction<any>;
		}
	}

	export type IProps = IOwnProps & IStateProps & IDispatchProps;

	export const mapStateToProps = (state: any, ownProp?: any): IStateProps => ({
		products: state.product.items
	});

	export const mapDispatchToProps = (dispatch: Dispatch): IDispatchProps => ({
		actionCreators: bindActionCreators({
			receiveProducts: Actions.productListRequest
		}, dispatch)
	});

	export const types: PropTypes.ValidationMap<IProps> = {
		actionCreators: PropTypes.shape({
			receiveProducts: PropTypes.func
		}),
		products: PropTypes.any
	}

	export const defaults: IProps = {
		//#region RouteComponentProps
		match: null,
		location: null,
		history: null,
		//#endregion
		actionCreators: {
			receiveProducts: undefined
		},
		products: []
	}
}
