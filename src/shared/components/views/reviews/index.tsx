"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { Partials, Stores } from "./../@imports";
import { ReviewsPageProps } from "./index-props";
import { ReviewsPageState } from "./index-state";
import ReviewList from "./iterable/list";

const styles = require("./index.scss");

const applyUpdateResult = (result: any) => (prevState: any) => ({
	hits: [...prevState.hits, ...result.hits],
	page: result.page
});

const applySetResult = (result: any) => (prevState: any) => ({
	hits: result.hits,
	page: result.page
});

export default class ReviewsPage extends React.Component<ReviewsPageProps.IProps, ReviewsPageState.IState> {
	//#region Статические переменные

	public static displayName: string = "ReviewsPage";
	public static propTypes: PropTypes.ValidationMap<any/*ReviewsPageProps.IProps*/> = ReviewsPageProps.types;
	public static defaultProps: ReviewsPageProps.IProps = ReviewsPageProps.defaults;

	//#endregion

	//#region Приватные переменные
	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class ReviewsPage
	 * @private
	 */
	private _getInitialState(): ReviewsPageState.IState {
		return {
			hits: [],
			page: null
		}
	}

	//#region Lazy

	private _onSetResult = (result: any, page: number, overwriting: boolean) => {
		(page === 0 || overwriting === false)
			? this.setState(applySetResult(result))
			: this.setState(applyUpdateResult(result));
	}

	private _fetchStories = (page: number) => {
		var entities = Stores.fakeStore.reviews.get(page);
		return this._onSetResult(entities, page, true);
	}

	private _onInitialSearch = () => this._fetchStories(0);
	private _onPaginatedSearch = (event: any) => this._fetchStories(this.state.page + 1);

	//#endregion

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class ReviewsPage
	 * @public
	 * @constructor
	 * @param {ReviewsPageProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: ReviewsPageProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Компонент примонтировался.
	 * В данный момент у нас есть возможность использовать refs.
	 * 
	 * @class ReviewsPage
	 * @public
	 */
	public componentDidMount() {
		this._onInitialSearch();
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class ReviewsPage
	 * @public
	 */
	public render(): JSX.Element {
		return (
			<div className={styles["container"]}>
				<ReviewList list={this.state.hits} page={this.state.page} onPaginatedSearch={this._onPaginatedSearch} />
			</div>
		);
	}
}
