"use strict";

import * as _ from "lodash";
import { EnumTransform } from "@timcowebapps/react.utils";
import { IAction } from "./../actions/action";
import { CostServiceTabset } from "./../stores/models/calc/cost-service";
import { SERVICE_CALC } from "./../actions/constants";
import { ActionTypes } from "./../actions/actiontypes";

export default function (state = CostServiceTabset.defaults, action: IAction<any>) {
	switch (action.type) {
		case SERVICE_CALC + EnumTransform.toStr(ActionTypes, ActionTypes.ADD):
			return state.length < 4
				? state.concat(_.merge({}, CostServiceTabset.defaults[0], { uid: action.payload }))
				: state;

		case SERVICE_CALC + EnumTransform.toStr(ActionTypes, ActionTypes.REMOVE):
			return state.length > 1
				? state.filter(i => i.uid !== action.payload)
				: state;

		default:
			return state;
	}
}
