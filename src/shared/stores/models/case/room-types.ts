"use strict";

export enum RoomTypes {
	Undefined = "Не определено",
	Livingroom = "Гостиная",
	Bathroom = "Ванная",
	Bedroom = "Спальня",
	Corridor = "Коридор",
	Kidsroom = "Детская",
	Balcony = "Балкон",
	Kitchen = "Кухня"
}
