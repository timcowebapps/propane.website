"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { Scrollable } from "@timcowebapps/react.toolkit";
import { Partials } from "./../@imports";
import { HomePageProps } from "./index-props";
import { HomePageState } from "./index-state";
import Front from "./sections/front/index";
import Calc from "./sections/calc/index";
import Portfolio from "./sections/portfolio/index";

//#region Import Assets

const styles = require("./index.scss");

//#endregion

export default class HomePage extends React.Component<HomePageProps.IProps, HomePageState.IState> {
	//#region Статические переменные

	public static displayName: string = "HomePage";
	public static propTypes: PropTypes.ValidationMap<HomePageProps.IProps> = HomePageProps.types;
	public static defaultProps: HomePageProps.IProps = HomePageProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class HomePage
	 * @private
	 */
	private _getInitialState(): HomePageState.IState {
		return {
			currentSectionIdx: 0
		}
	}

	/**
	 * Реакция на изменение индекса текущей секции.
	 * 
	 * @class HomePage
	 * @private
	 * @param {number} sectionIdx Индекс текущей секции.
	 */
	private _handleSectionChange(sectionIdx: number) {
		this.setState({ currentSectionIdx: sectionIdx });
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class HomePage
	 * @public
	 * @constructor
	 * @param {HomePageProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: HomePageProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		//#region Bindngs

		this._handleSectionChange = this._handleSectionChange.bind(this);

		//#endregion
	}

	/**
	 * Компонент будет примонтирован.
	 * В данный момент у нас нет возможности посмотреть DOM элементы.
	 * 
	 * @class HomePage
	 * @public
	 */
	public componentWillMount(): void {
		// Empty
	}

	/**
	 * Компонент примонтировался.
	 * В данный момент у нас есть возможность использовать refs.
	 * 
	 * @class HomePage
	 * @public
	 */
	public componentDidMount(): void {
		// Empty
	}

	/**
	 * Вызывается сразу перед тем, как компонент будет удален из DOM.
	 * 
	 * @class HomePage
	 * @public
	 */
	public componentWillUnmount(): void {
		// Empty
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class HomePage
	 * @public
	 */
	public render(): JSX.Element {
		return (
			<Scrollable {...{
				defaults: { currentItemIdx: 0 },
				requirements: {
					useHash: false,
					current: this.state.currentSectionIdx,
					onChange: this._handleSectionChange,
					transitionDelay: 2000,
					viewStyle: { stylesheet: styles, bem: { block: "scrollable" } }
				},
				items: [Front, Calc, Portfolio].map((sectionComp: any, idx: number) => {
					return {
						uid: sectionComp.displayName.toLowerCase(),
						requirements: {
							children: React.createElement(sectionComp, {
								isCurrSection: this.state.currentSectionIdx === idx,
								onSectionChange: this._handleSectionChange
							})
						}
					}
				})
			}} />
		);
	}
}
