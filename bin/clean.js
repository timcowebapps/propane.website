"use strict";

const path = require("path");
const del = require("del");
const pkgConfig = require("./../package.config.js");

const items = [
	path.resolve(__dirname, "../wwwroot/")
];

(async () => {
	const deletedPaths = await del(items);
	console.log("Deleted files and directories:\n", deletedPaths.join("\n"));
})();
