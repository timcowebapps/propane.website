"use strict";

export namespace HomePageState {
	export interface IState {
		currentSectionIdx: number;
	}
}
