"use strict";

import * as PropTypes from "prop-types";
import {ViewStyleProps}from "@timcowebapps/react.style";

export namespace EmptyCartProps {
	export interface IProps {
		// Empty
	}

	export const types = {
		requirements: PropTypes.shape({
			viewStyle: PropTypes.shape(ViewStyleProps.types)
		})
	};

	export const defaults = {
		requirements: {
			viewStyle: ViewStyleProps.defaults("cart", "empty")
		}
	};
}
