"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Guid } from "@timcowebapps/react.system";
import { Methodology } from "@timcowebapps/react.style";
import { EnumHelpers, HtmlTagTypes } from "@timcowebapps/react.utils";
import { Heading, Price } from "@timcowebapps/react.toolkit";
import { RouteConstants } from "./../../../../../routeconstants";
import { Partials, Stores, Models } from "./../../../@imports";
import { CalcProps } from "./index-props";
import { CalcState } from "./index-state";

import { HeadingVariants } from "./../../../../ui/typography/heading/variants";

//#region Import Assets

const styles = require("./../../index.scss");

//#endregion

export default connect(CalcProps.mapStateToProps, CalcProps.mapDispatchToProps)(
	class Calc extends React.Component<CalcProps.IProps, CalcState.IState> {
		//#region Статические переменные

		public static displayName: string = "Calc";
		public static propTypes: PropTypes.ValidationMap<CalcProps.IProps> = CalcProps.types;
		public static defaultProps: CalcProps.IProps = CalcProps.defaults;

		//#endregion

		//#region Приватные переменные

		private _pricelist: Models.ICost = null;

		//#endregion

		//#region Приватные методы

		/**
		 * Начальное состояние свойств по умолчанию.
		 * 
		 * @class Calc
		 * @private
		 */
		private _getInitialState(): CalcState.IState {
			return {
				area: 15,
				number_corners: 4,
				number_pipes: 0,
				selected_material: Models.MaterialTypes.PVH,
				selected_room: Object.keys(Models.RoomTypes).filter(x => Models.RoomTypes[x].includes(Models.RoomTypes.Bedroom))[0]
			}
		}

		private _onChangeHandler(change: any): void {
			this.setState({ area: change });
		}

		//#endregion

		/**
		 * Конструктор класса.
		 * 
		 * @class Calc
		 * @public
		 * @constructor
		 * @param {CalcProps.IProps} props Свойства компонента.
		 */
		public constructor(props?: CalcProps.IProps) {
			super(props);

			this.state = this._getInitialState();

			//#region Bindings

			this._onChangeHandler = this._onChangeHandler.bind(this);

			//#endregion

			this._pricelist = Stores.fakeStore.pricelist.get();
		}

		/**
		 * Отрисовывает компонент.
		 * 
		 * @class Calc
		 * @public
		 */
		public render(): JSX.Element {
			/** Цена полотна 1м2. */
			const canvas = EnumHelpers.compareWithNum(Models.MaterialTypes, Models.MaterialTypes.Cloth, this.state.selected_material) ? this._pricelist.canvas.cloth : this._pricelist.canvas.pvh;
			/** Цена полотна 1м2 с работой. */
			const canvasWithWork = canvas + this._pricelist.canvas.montage;
			/** Площадь потолка. */
			const ceilingArea = Number((Math.round(this.state.area * 10) / 10).toFixed(0));
			/** Отдельно полотно. */
			const separatelyCanvas = ceilingArea * canvas;
			/** Итоговая стоимости потолка с установкой. */
			const total = (ceilingArea * canvasWithWork) + (this.state.number_corners * this._pricelist.corner);

			return (
				<div className={styles["container"]}>
					<div className={styles["row"]}>
						<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
							<Heading {...HeadingVariants.getSectionTitleSheme(styles, "Предварительный расчет стоимости вашего потока")} />
						</div>
					</div>
					<div className={styles["row"]}>
						<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-6", "lg-6"])}>
							{/* <p>Воспользуйтесь данным калькулятором, чтобы сделать предварительный расчет стоимости натяжного потолка. Получить окончательную смету по стоимости Вы можете вызвав замерщика.</p> */}

							<Partials.CustomSlider {...{
								uid: Guid.newGuid(),
								requirements: {
									name: "area",
									min: 0, max: 50, step: 1,
									value: this.state.area,
									unit: <React.Fragment>м<sup>2</sup></React.Fragment>,
									onChanged: this._onChangeHandler
								},
								items: [{
									uid: "label",
									requirements: { text: "Площадь помещения" }
								}, {
									uid: "input",
									requirements: {}
								}, {
									uid: "sumup",
									requirements: {
										piece: "Отдельно полотно",
										total: <React.Fragment>от&nbsp;
											<Price {...{
												requirements: {
													htmlTag: HtmlTagTypes.Span,
													amount: separatelyCanvas, currency: "руб.",
													viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["sale"] } }
												}
											}} />
										</React.Fragment>
									}
								}]
							}} />

							<Link className={Methodology.Bem.Entities.block(styles, "btn").element().modifiers(["success"])} to={RouteConstants.Calc}>Рассчитать подробнее</Link>

						</div>
						<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-6", "lg-6"])}>
							{/* <small>Нажимая кнопку "Заказать" Вы выражаете согласие на обработку персональных данных в соответсвии с законом  ФЗ-152 «О персональных данных» от 27.07.2006 и принимаете условия <a href="/">Пользовательского соглашения</a></small> */}
						</div>
					</div>
					<div className={styles["row"]}>
						<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
							{/* <small>Данный расчет является предварительным и не учитывает все характеристики Вашего потолка. Для получения более точного расчета цены натяжного потолка Вы можете бесплатно вызвать нашего замерщика, он сделает замер потолка и расчет стоимости натяжного потолка.</small> */}
							<div className={styles["price"]}>Стоимость потолка с установкой:&nbsp;<span className={Methodology.Bem.Entities.block(styles, "price").element("value").toStr()}>{total}</span>&nbsp;<span className={Methodology.Bem.Entities.block(styles, "price").element("cur").toStr()}>руб.</span></div>
						</div>
					</div>
				</div>
			);
		}
	}
);
