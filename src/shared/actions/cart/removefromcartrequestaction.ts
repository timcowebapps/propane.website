"use strict";

import { Action, ActionCreator } from "redux";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { RemoveFromCartActionTypeState } from "./removefromcartactiontypestate";

export const removeFromCartRequestAction: ActionCreator<Action> = (id: string): IAction<any> => {
	return {
		type: RemoveFromCartActionTypeState.toStr(ActionTypeStates.REQUEST),
		payload: {
			productId: id
		}
	}
}

export const removeFromCartRequest = removeFromCartRequestAction;
