"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { Methodology } from "@timcowebapps/react.style";
import { ReviewListItemProps } from "./list-item-props";
import { ReviewListItemState } from "./list-item-state";

const styles = require("./../index.scss");

export default class ReviewListItem extends React.Component<ReviewListItemProps.IProps, ReviewListItemState.IState> {
	//#region Статические переменные

	public static displayName: string = "ReviewListItem";
	public static propTypes: PropTypes.ValidationMap<any/*ReviewListItemProps.IProps*/> = ReviewListItemProps.types;
	public static defaultProps: ReviewListItemProps.IProps = ReviewListItemProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class ReviewListItem
	 * @private
	 */
	private _getInitialState(): ReviewListItemState.IState {
		return {
			// Empty
		}
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class ReviewListItem
	 * @public
	 * @constructor
	 * @param {ReviewListItemProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: ReviewListItemProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class ReviewListItem
	 * @public
	 */
	public render(): JSX.Element {
		const { entity } = this.props;
		const blockRef: any = React.createRef();

		return (
			<div ref={blockRef} onClick={() => {
				blockRef.current.scrollIntoView({
					behavior: "smooth",
					block: "start"
				})
			}} className={styles["review_block"]} itemScope itemType="http://schema.org/Review">
				<div className={styles["review_block_inner"]}>
					<div className={Methodology.Bem.Entities.block(styles, "review_block").element("advantages").toStr()}><strong>Преимущества:&nbsp;</strong>{entity.advantages}</div>
					<div className={Methodology.Bem.Entities.block(styles, "review_block").element("disadvantages").toStr()}><strong>Недостатки:&nbsp;</strong>{entity.disadvantages}</div>
					<div className={Methodology.Bem.Entities.block(styles, "review_block").element("text").toStr()} itemProp="reviewBody"><strong>Комментарий:&nbsp;</strong>{entity.text}</div>
				</div>
				<div className={styles["review_block_info"]}>
					<span className={Methodology.Bem.Entities.block(styles, "review_block_info").element("autor-img").toStr()} itemScope itemType="http://schema.org/ImageObject"><img src="https://via.placeholder.com/102x102" itemProp="contentUrl" alt="" /></span>
					<span className={Methodology.Bem.Entities.block(styles, "review_block_info").element("autor").toStr()}>{entity.name}</span>
					,&nbsp;
					<span className={Methodology.Bem.Entities.block(styles, "review_block_info").element("date").toStr()}><meta itemProp="datePublished" content={entity.date} />{entity.date}&nbsp;года</span>
					,&nbsp;
					<span className={Methodology.Bem.Entities.block(styles, "review_block_info").element("city").toStr()}>{entity.city}</span>
				</div>

				<div style={{ "display": "none" }} itemProp="itemReviewed" itemScope itemType="http://schema.org/Organization" >
					<span itemProp="name">Propane</span>
					<div className="phone-block left">
						<div className="phone-text" itemProp="telephone"><span>+7 (952) </span>666-52-22</div>
						<div className="feedback-link"><a href="#">Заказать звонок</a></div>
					</div>
					<div className="address-block right" itemProp="address" itemScope itemType="http://schema.org/PostalAddress">
						<div className="city" itemProp="addressLocality">Санкт-Петербург</div>
						<div className="address-text">
							<span itemProp="streetAddress">Не указан</span>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
