"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { Methodology } from "@timcowebapps/react.style";
import { CN } from "@timcowebapps/react.utils";
import Aside from "./../partials/aside/index";
import { LayoutProps } from "./index-props";
import { LayoutState } from "./index-state";

const styles = require("./index.scss");

export class Layout extends React.Component<LayoutProps.IProps, LayoutState.IState> {
	//#region Статические переменные

	public static displayName: string = "Layout";
	public static propTypes: PropTypes.ValidationMap<LayoutProps.IProps> = LayoutProps.types;
	public static defaultProps: LayoutProps.IProps = LayoutProps.defaults;

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Layout
	 * @public
	 * @constructor
	 * @param {LayoutProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: LayoutProps.IProps) {
		super(props);
	}

	/**
	 * Компонент примонтировался.
	 * В данный момент у нас есть возможность использовать refs.
	 * 
	 * @class Layout
	 * @public
	 */
	public componentDidMount() {
		/** Устанавливаем заголовок документа */
		document.title = "Натяжные потолки в Санкт-Петербурге и Лен. области";
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Layout
	 * @public
	 */
	public render(): JSX.Element {
		return (
			<React.Fragment>
				<main>
					{/* Содержимое страницы */}
					{this.props.children}
					{/* Фоновые вертикальные линии */}
					<div className={CN.many(styles["container"], styles["zebra"])}>
						{Array.apply(0, Array(6)).map((x: any, i: number) => <div key={i} className={Methodology.Bem.Entities.block(styles, "zebra").element("line").toStr()} />)}
					</div>
				</main>
				<Aside />
			</React.Fragment>
		);
	}
}
