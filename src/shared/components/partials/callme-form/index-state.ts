"use strict";

export namespace CallmeFormState {
	export interface IState {
		username: string;
		phone: string;
	}
}
