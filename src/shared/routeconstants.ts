"use strict";

export namespace RouteConstants {
	export const Home = "/";
	export const Calc = "/calc";
	export const Shop = "/shop";
	export const Reviews = "/reviews";
}
