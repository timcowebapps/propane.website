"use strict";

import * as PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { IAction } from "./../../../actions/action";
import { Actions } from "./../../../sagas/@exports";
import { ICostServicePayload } from "./../../../stores/models/calc/cost-service";

export namespace CalcProps {
	export interface IOwnProps extends React.Props<any> {
		// Empty
	}

	export interface IStateProps {
		calc: any;
	}

	export interface IDispatchProps {
		actionCreators: {
			//addRoom?: (uid: string) => any;
			//removeRoom?: (uid: string) => any;
			changeCalcData?: (payload: ICostServicePayload) => IAction<any>;
		}
	}

	export type IProps = IOwnProps & IStateProps & IDispatchProps;

	export const mapStateToProps = (state: any, ownProp?: any): IStateProps => ({
		calc: state.calculator
	});

	export const mapDispatchToProps = (dispatch: any): IDispatchProps => ({
		actionCreators: bindActionCreators({
			//addRoom, removeRoom,
			changeCalcData: Actions.changeDataRequest
		}, dispatch)
	});

	export const types: PropTypes.ValidationMap<IProps> = {
		calc: PropTypes.any,
		actionCreators: PropTypes.shape({
			//addRoom: PropTypes.func,
			//removeRoom: PropTypes.func,
			changeCalcData: PropTypes.func
		}).isRequired
	}

	export const defaults: IProps = {
		calc: null,
		actionCreators: {
			//addRoom: undefined,
			//removeRoom: undefined,
			changeCalcData: undefined
		}
	}
}
