"use strict";

import { IAction, ActionTypeStates, Actions } from "./../../sagas/@exports";
import { ICartReducerState } from "./cartreducerstate";

export function cartReducer(state = {
	items: []
} as ICartReducerState, action: IAction<any>) {
	switch (action.type) {

		//#region Добавление продукта в корзину

		case Actions.AddToCartActionTypeState.toStr(ActionTypeStates.REQUEST):
		case Actions.AddToCartActionTypeState.toStr(ActionTypeStates.FAILURE):
			return state;

		case Actions.AddToCartActionTypeState.toStr(ActionTypeStates.SUCCESS):
			return { items: [ ...state.items, action.payload.item] }

		//#endregion

		//#region Удаление продукта из корзины

		case Actions.RemoveFromCartActionTypeState.toStr(ActionTypeStates.REQUEST):
		case Actions.RemoveFromCartActionTypeState.toStr(ActionTypeStates.FAILURE):
			return state;

		case Actions.RemoveFromCartActionTypeState.toStr(ActionTypeStates.SUCCESS):
			return state.items.filter(i => i.id !== action.payload.productId)

		//#endregion

		default:
			return state;
	}
}
