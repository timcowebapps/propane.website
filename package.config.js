"use strict";

module.exports = {
	devbuild: ((process.env.NODE_ENV || "development").trim().toLowerCase() === "development"),
	disabledChunkhash: true,
	realport: parseInt(process.env.PORT) || 2024,
	dev: {
		contentPath: "./wwwroot/",
		host: "0.0.0.0",
		port: 8081,
		publicPath: "http://0.0.0.0:8081/dst/"
	},
	paths: {
		src: {
			base: "./src/",
			tmpl: "./src/templates/index.cshtml"
		},
		dst: {
			base: "./wwwroot/dst/"
		}
	},
	urls: {
		publicPath: "./dst/",
	}
}
