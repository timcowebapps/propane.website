"use strict";

import * as _ from "lodash";
import * as React from "react";
import * as PropTypes from "prop-types";
import { Guid } from "@timcowebapps/react.system";
import { Methodology } from "@timcowebapps/react.style";
import { Data } from "@timcowebapps/react.componentmodel";
import { HtmlTagTypes, HtmlInputTypes, OrientTypes } from "@timcowebapps/react.utils";
import { FormGroup, RangeSlider } from "@timcowebapps/react.toolkit";
import { CustomSliderProps } from "./index-props";
import { CustomSliderState } from "./index-state";

const styles = require("./index.scss");
const labelStyles = require("./../../../ui/label/variants.scss");
const formGroupStyles = require("./../../../ui/form-group/variants.scss");
const inputStyles = require("./../../../ui/input/variants.scss");

export class CustomSlider extends React.Component<CustomSliderProps.IProps, CustomSliderState.IState> {
	//#region Статические переменные

	public static displayName: string = "CustomSlider";
	public static propTypes: PropTypes.ValidationMap<CustomSliderProps.IProps> = CustomSliderProps.types;
	public static defaultProps: CustomSliderProps.IProps = CustomSliderProps.defaults;

	//#endregion

	//#region Приватные переменные

	private _rangeUid: string = Guid.empty;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class CustomSlider
	 * @private
	 */
	private _getInitialState(): CustomSliderState.IState {
		return {
			data: this.props.requirements.value
		}
	}

	//#region Events

	private _handleFieldChanged(event: React.FormEvent<HTMLInputElement>, value: string): void {
		this.setState({
			data: parseInt(value)
		}, _.invoke(this.props.requirements, "onChanged", parseInt(value)));
	}

	private _handleRangeChanged(value: number): void {
		this.setState({
			data: value
		}, _.invoke(this.props.requirements, "onChanged", value));
	}

	//#endregion

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class CustomSlider
	 * @public
	 * @constructor
	 * @param {CustomSliderProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: CustomSliderProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._rangeUid = Guid.newGuid();

		this._handleFieldChanged = this._handleFieldChanged.bind(this);
		this._handleRangeChanged = this._handleRangeChanged.bind(this);
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class CustomSlider
	 * @public
	 */
	public render(): JSX.Element {
		const { uid, requirements, items } = this.props;

		const labelScheme: Data.Schema.IComponentProps = Data.Schema.ComponentPropSetHelpers.getItemById(items, "label");
		const inputScheme: Data.Schema.IComponentProps = Data.Schema.ComponentPropSetHelpers.getItemById(items, "input");
		const sumupScheme: Data.Schema.IComponentProps = Data.Schema.ComponentPropSetHelpers.getItemById(items, "sumup");

		return React.createElement("custom-slider", { class: styles["form-row"] },
			<React.Fragment>
				<div className={Methodology.Bem.Entities.block(styles, "form-row").element("input").toStr()}>
					<FormGroup {...{
						requirements: {
							viewStyle: { stylesheet: formGroupStyles, bem: { block: "rangeslider__form-group" } }
						},
						items: [{
							uid: "label",
							type: Data.Schema.ComponentTypes.Label,
							requirements: {
								text: labelScheme.requirements.text,
								viewStyle: { stylesheet: labelStyles }
							}
						}, {
							type: Data.Schema.ComponentTypes.NumInput,
							requirements: {
								viewStyle: { stylesheet: styles }
							},
							items: [{
								uid: uid,
								type: Data.Schema.ComponentTypes.Input,
								requirements: {
									htmlType: HtmlInputTypes.Number,
									name: requirements.name,
									value: this.state.data.toString(),
									validations: [],
									onValueChange: this._handleFieldChanged,
									viewStyle: { stylesheet: styles, bem: { block: "num-input", element: "input" } }
								}
							}]
						}]
					}} />

					<div className={styles["unit-area"]}>{requirements.unit}</div>

					<div className={Methodology.Bem.Entities.block(styles, _.join(["form-row", "input"], "__")).element("sumup").toStr()}>
						<div className={Methodology.Bem.Entities.block(styles, _.join(["form-row", "input", "sumup"], "__")).element("piece-price").toStr()}>{sumupScheme.requirements.piece}</div>
						<div className={Methodology.Bem.Entities.block(styles, _.join(["form-row", "input", "sumup"], "__")).element("total-price").toStr()}>{sumupScheme.requirements.total}</div>
					</div>
				</div>
				<div className={Methodology.Bem.Entities.block(styles, "form-row").element("slider").toStr()}>
					<RangeSlider key={this._rangeUid + "-area-customslider"} {...{
						uid: this._rangeUid,
						requirements: {
							htmlId: requirements.htmlId,
							orientation: OrientTypes.Horizontal,
							min: requirements.min, max: requirements.max,
							step: requirements.step,
							value: this.state.data,
							onChange: this._handleRangeChanged,
							viewStyle: { stylesheet: styles, bem: { block: "rangeslider" } }
						}
					}} />
				</div>
			</React.Fragment>
		);
	}
}
