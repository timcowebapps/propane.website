"use strict";

import * as PropTypes from "prop-types";
import { ISectionProps } from "./../section-props";

export namespace PortfolioProps {
	export interface IProps extends ISectionProps {
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		isCurrSection: PropTypes.bool.isRequired,
		onSectionChange: PropTypes.func
	}

	export const defaults: IProps = {
		isCurrSection: false,
		onSectionChange: undefined
	}
}
