"use strict";

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/firestore";
import { firebaseConfig } from "./firebaseconfig";

export class FirebaseService {
	private static _service: FirebaseService;
	public static get instance(): FirebaseService {
		if (!this._service)
			this._service = new FirebaseService();

		return this._service;
	}

	public readonly firebase : firebase.app.App;
	public readonly firestore : firebase.firestore.Firestore;

	constructor() {
		this.firebase = firebase.initializeApp(firebaseConfig);
		this.firestore = this.firebase.firestore();
		//this.firestore.settings({ timestampsInSnapshots: true });

		// this.firestore.collection("products").add({
		// 	brand: "Ecola",
		// 	model: "H4 2 color",
		// 	thumb: "https://via.placeholder.com/200x200/fff",
		// 	price: {
		// 		original: 257,
		// 		sale: 240
		// 	},
		// 	specs: {
		// 		material: "glass",
		// 		color: "chrome",
		// 		cap: "GX53",
		// 		dimensions: {
		// 			diameter: 106,
		// 			depth: 38,
		// 			cut: 90
		// 		}
		// 	},
		// 	params: {
		// 		voltage: "220-240",
		// 		frequency: "50-60"
		// 	}
		// });
	}
}
