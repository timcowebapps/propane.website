"use strict";

const path = require("path");
const webpack = require("webpack");
const merge = require("webpack-merge");
const CopyPlugin = require("copy-webpack-plugin");
const ManifestPlugin = require("webpack-manifest-plugin");
const ReactLoadablePlugin = require("react-loadable/webpack").ReactLoadablePlugin;
const HtmlPlugin = require("html-webpack-plugin");
const pkg = require("./../package.json");
const pkgConfig = require("./../package.config.js");

module.exports = merge(require("./webpack.config.base.js"), {
	name: "dev.client",
	mode: "development",
	devtool: "cheap-module-source-map",
	entry: {
		"bundle.client": [
			`webpack-dev-server/client?http://${pkgConfig.dev.host}:${pkgConfig.dev.port}`,
			"webpack/hot/only-dev-server",
			path.resolve(__dirname, "../", pkgConfig.paths.src.base, "client")
		]
	},
	output: {
		filename: "[name].js",
		publicPath: pkgConfig.dev.publicPath
	},
	plugins: [
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: JSON.stringify("development"),
				BROWSER: JSON.stringify(true),
				VERSION: JSON.stringify(pkg.version)
			}
		}),
		new webpack.ProvidePlugin({
			"React": "react",
			"ReactDOM": "react-dom"
		}),
		new CopyPlugin([
			{
				from: path.resolve(__dirname, "../assets/"),
				to: path.resolve(__dirname, "../wwwroot/")
			}, {
				from: path.resolve(__dirname, "../src/manifest.json"),
				to: path.resolve(__dirname, "../wwwroot/")
			}, {
				from: path.resolve(__dirname, "../src/robots.txt"),
				to: path.resolve(__dirname, "../wwwroot/")
			}, {
				from: path.resolve(__dirname, "../src/sitemap.xml"),
				to: path.resolve(__dirname, "../wwwroot/")
			}
		]),
		new ManifestPlugin({
			fileName: path.resolve(__dirname, "../wwwroot/asset-manifest.json")
		}),
		new ReactLoadablePlugin({
			filename: path.resolve(__dirname, "../wwwroot/react-loadable.json")
		}),
		new HtmlPlugin({
			title: pkg.name + ' - ' + pkg.description,
			filename: path.resolve(__dirname, "../wwwroot/index.cshtml"),
			template: path.resolve(__dirname, "../", pkgConfig.paths.src.tmpl),
			//excludeChunks: ["manifest"],
			inject: true,
			injectComponent: {
				markup: "@html.raw(model.markup)",
				scripts: "@html.raw(model.scripts.join())",
				styles: "@html.raw(model.styles.join())",
				preloadedState: "@html.raw(model.preloadedState)"
			},
			minify: false
		}),
		new webpack.HotModuleReplacementPlugin()
	]
});
