"use strict";

import { Action, ActionCreator } from "redux";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { RemoveFromCartActionTypeState } from "./removefromcartactiontypestate";

export const removeFromCartSuccessAction: ActionCreator<Action> = (id: any): IAction<any> => {
	return {
		type: RemoveFromCartActionTypeState.toStr(ActionTypeStates.SUCCESS),
		payload: {
			id: id
		}
	};
}

export const removeFromCartSuccess = removeFromCartSuccessAction;
