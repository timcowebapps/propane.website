"use strict";

import { Action, ActionCreator } from "redux";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { RemoveFromCartActionTypeState } from "./removefromcartactiontypestate";

export const removeFromCartFailureAction: ActionCreator<Action> = (error: any): IAction<any> => {
	return {
		type: RemoveFromCartActionTypeState.toStr(ActionTypeStates.FAILURE),
		payload: {
			error: error
		}
	};
}

export const removeFromCartFailure = removeFromCartFailureAction;
