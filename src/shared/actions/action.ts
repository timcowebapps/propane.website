"use strict";

export type IAction<T extends {}> = T & {
	/** @brief Тип действия. */
	type: string;

	/** @brief Дополнительные данные. */
	payload?: T;
}
