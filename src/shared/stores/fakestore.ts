"use strict";

import { FirebaseService } from "./../services/firebaseservice";

namespace constants {
	export const TIMEOUT = 100;
}

export const fakeStore = {
	//#region Репозиторий продукта

	"pricelist": {
		get: () => require("./datasets/pricelist.json")
	},

	//#endregion

	//#region Отзывы

	"reviews": {
		gets: () => require("./datasets/reviews.json"),
		get: (page_number: number) => {
			const page_size = 1;
	
			return {
				hits: require("./datasets/reviews.json").slice(page_number * page_size, (page_number + 1) * page_size),
				page: page_number
			};
		}
	}

	//#endregion
}
