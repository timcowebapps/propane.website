"use strict";

const path = require("path");

module.exports = {
	"timcowebapps-react.utils": path.resolve(__dirname, "./../node_modules/@timcowebapps/react.utils"),
	"timcowebapps-react.style": path.resolve(__dirname, "./../node_modules/@timcowebapps/react.style"),
	"timcowebapps-react.toolkit": path.resolve(__dirname, "./../node_modules/@timcowebapps/react.toolkit")
};
