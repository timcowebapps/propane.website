"use strict";

import * as Models from "./../../../../stores/models/@export-models";

export namespace CustomTabPaneState {
	export interface IState {
		data: Models.ICostServicePayload;
	}
}
