"use strict";

import * as React from "react";
import * as Loadable from "react-loadable";
import { StaticRouter, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import { Routes } from "./routes";
import { RoutePolicy } from "./../shared/routepolicy";
import { Layout } from "./../shared/components/layout";

export const Root = ({ request, store, context, modules }: { request: any, store: any, context: any, modules: any }) => (
	<Loadable.Capture report={moduleName => modules.push(moduleName)}>
		<Provider store={store} key={Math.random()}>
			<StaticRouter location={request.url} context={context} key={Math.random()}>
				<Layout>
					<Switch>
						{Routes.map((route) => <RoutePolicy component={route.component} {...route} key={route.key} />)}
					</Switch>
				</Layout>
			</StaticRouter>
		</Provider>
	</Loadable.Capture>
);
