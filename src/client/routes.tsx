"use strict";

import { RouteConstants } from "./../shared/routeconstants";
import Home from "./../shared/components/views/home/homeasync";
import Calc from "./../shared/components/views/calc/loadable";
import Shop from "./../shared/components/views/shop/loadable";
import Reviews from "./../shared/components/views/reviews/loadable";
import NotFound from "./../shared/components/views/notfound/loadable";

export const Routes = [
	{
		key: "home",
		exact: true,
		path: RouteConstants.Home,
		component: Home
	}, {
		key: "calc",
		exact: true,
		path: RouteConstants.Calc,
		component: Calc
	}, {
		key: "shop",
		exact: true,
		path: RouteConstants.Shop,
		component: Shop
	}, {
		key: "reviews",
		exact: true,
		path: RouteConstants.Reviews,
		component: Reviews
	}, {
		key: "notFound",
		exact: false,
		path: "*",
		component: NotFound
	}
];
