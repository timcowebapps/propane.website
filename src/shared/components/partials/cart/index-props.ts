"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import * as Models from "./../../../stores/models/@export-models";
import { IAction } from "./../../../actions/action";
import { Actions } from "./../../../sagas/@exports";

export namespace CartProps {
	export interface IOwnProps extends React.Props<any> {
		// Empty
	}

	export interface IStateProps {
		items: Models.IProduct[];
	}

	export interface IDispatchProps {
		actionCreators: {
			removeFromCart?: (productId: number | string) => IAction<any>;
		}
	}

	export type IProps = IOwnProps & IStateProps & IDispatchProps;

	export const mapStateToProps = (state: any, ownProp?: any): IStateProps => ({
		items: state.cart.items
	});

	export const mapDispatchToProps = (dispatch: any): IDispatchProps => ({
		actionCreators: bindActionCreators({
			removeFromCart: Actions.removeFromCartRequest
		}, dispatch)
	});

	export const types: PropTypes.ValidationMap<IProps> = {
		items: PropTypes.arrayOf(PropTypes.any),
		actionCreators: PropTypes.shape({
			removeFromCart: PropTypes.func
		})
	}

	export const defaults: IProps = {
		items: [],
		actionCreators: {
			removeFromCart: undefined
		}
	}
}
