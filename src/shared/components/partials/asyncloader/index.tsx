import * as React from "react";

export interface IProps {}
interface IState {}

export default class AsyncLoader extends React.Component<IProps & LoadableExport.LoadingComponentProps, IState> {
	public render (): JSX.Element | null {
		if (this.props.error) {
			return <h3>Count not load content. <button onClick={this.props.retry}>Retry</button></h3>
		} else if (this.props.timedOut) {
			return <h3>Taking longer than expected... <button onClick={this.props.retry}>Retry</button></h3>
		} else if (this.props.pastDelay) {
			return <h3>Loading...</h3>
		} else {
			return null;
		}
	}
}
