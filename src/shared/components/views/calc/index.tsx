"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { connect } from "react-redux";
import { Methodology } from "@timcowebapps/react.style";
import { Data } from "@timcowebapps/react.componentmodel";
import { CN, EnumHelpers, HtmlTagTypes, AlignTypes, AlignTransform } from "@timcowebapps/react.utils";
import { Heading, Button, Price } from "@timcowebapps/react.toolkit";
import CustomTabPane from "./../../partials/calc-components/custom-tabpane";
import { Calculator } from "./../../partials/calc-components/calculator";
import { Partials, Stores, Models } from "./../@imports";
import { CalcProps } from "./index-props";
import { CalcState } from "./index-state";

//#region Import Assets

const styles = require("./index.scss");

//#endregion

export default connect(CalcProps.mapStateToProps, CalcProps.mapDispatchToProps)(
	class Calc extends React.Component<CalcProps.IProps, CalcState.IState> {
		//#region Статические переменные

		public static displayName: string = "Calc";
		public static propTypes: PropTypes.ValidationMap<CalcProps.IProps> = CalcProps.types;
		public static defaultProps: CalcProps.IProps = CalcProps.defaults;

		//#endregion

		//#region Приватные переменные

		private _pricelist: Models.ICost = null;
		private _calculator: Calculator = null;

		//#endregion

		//#region Приватные методы

		/**
		 * Начальное состояние свойств по умолчанию.
		 * 
		 * @class Calc
		 * @private
		 */
		private _getInitialState(): CalcState.IState {
			return {
				currentTabIdx: this.props.calc.length - 1
			}
		}

		//#region Events

		private _onChangeHandler(change: Models.ICostServicePayload): void {
			this.props.actionCreators.changeCalcData(change);
		}

		//#endregion

		//#endregion

		/**
		 * Конструктор класса.
		 * 
		 * @class Calc
		 * @public
		 * @constructor
		 * @param {CalcProps.IProps} props Свойства компонента.
		 */
		public constructor(props?: CalcProps.IProps) {
			super(props);

			this.state = this._getInitialState();

			//#region Bindings

			this._onChangeHandler = this._onChangeHandler.bind(this);

			//#endregion

			this._pricelist = Stores.fakeStore.pricelist.get();
			this._calculator = new Calculator(this._pricelist);
		}

		/**
		 * Отрисовывает компонент.
		 * 
		 * @class Calc
		 * @public
		 */
		public render(): JSX.Element {
			/** Цена за монтаж 1м2 */
			const montage = this._pricelist.canvas.montage;

			/** Цена полотна 1м2. */
			const canvas = EnumHelpers.compareWithNum(Models.MaterialTypes, Models.MaterialTypes.Cloth, this.props.calc[this.state.currentTabIdx].payload.selected_material) ? this._pricelist.canvas.cloth : this._pricelist.canvas.pvh;
			/** Цена полотна 1м2 с работой. */
			const canvasWithWork = canvas + montage;

			/** Площадь потолка. */
			const ceilingArea = Number((Math.round(this.props.calc[this.state.currentTabIdx].payload.area * 10) / 10).toFixed(0));

			const corners = this.props.calc[this.state.currentTabIdx].payload.number_corners * this._pricelist.corner;
			const pipes = this.props.calc[this.state.currentTabIdx].payload.number_pipes * this._pricelist.pipe;
			const lusters = this.props.calc[this.state.currentTabIdx].payload.number_lusters * this._pricelist.luster;
			const lamps = this.props.calc[this.state.currentTabIdx].payload.number_lamps * this._pricelist.lamp;

			const totalCost = (ceilingArea * canvasWithWork) + corners + pipes + lusters + lamps;

			return (
				<div className={styles["container"]}>
					<div className={
						CN.many(
							styles["row"],
							Methodology.Bem.Entities.block(styles, "spacing-above").element().modifiers(["lg"]),
							Methodology.Bem.Entities.block(styles, "spacing-below").element().modifiers(["sm"])
						)}>
						<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
							<Heading {...{
								requirements: {
									htmlTag: HtmlTagTypes.H1,
									align: AlignTransform.toStr(AlignTypes.Left),
									viewStyle: { stylesheet: styles, bem: { block: "heading" } }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Node,
									requirements: { content: "Калькулятор стоимости потолка" }
								}]
							}} />

							<small>Воспользуйтесь данным калькулятором, чтобы сделать предварительный расчет стоимости натяжного потолка. Получить окончательную смету по стоимости Вы можете вызвав замерщика.</small>
						</div>
					</div>
					<div className={styles["row"]}>
						<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-6", "lg-6"])}>

							<CustomTabPane initialValues={this.props.calc[this.state.currentTabIdx].payload} pricelist={this._pricelist} onChangeHandler={this._onChangeHandler} />

						</div>
						<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-6", "lg-6"])}>
							<div className={styles["price"]}>
								Стоимость потолка с установкой:&nbsp;
								<Price {...{
									requirements: {
										htmlTag: HtmlTagTypes.Span,
										amount: totalCost, currency: "руб.",
										viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["sale"] } }
									}
								}} />
							</div>

							<Button {...{
								requirements: {
									htmlTag: HtmlTagTypes.Button,
									onClick: (event: any) => { },
									viewStyle: { stylesheet: styles, bem: { block: "btn", modifiers: ["primary"] }, extracts: "block" }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Node,
									requirements: { content: "Заказать замер" }
								}]
							}} />
							<small>Нажимая кнопку "Заказать" я соглашаюсь на обработку своих персональных данных.</small>
						</div>
					</div>
					<div className={CN.many(
						styles["row"],
						Methodology.Bem.Entities.block(styles, "spacing-below").element().modifiers(["lg"])
					)}>
						<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
							<small>Данный расчет является предварительным и не учитывает все характеристики Вашего потолка. Для получения более точного расчета цены натяжного потолка Вы можете бесплатно вызвать нашего замерщика, он сделает замер потолка и расчет стоимости натяжного потолка.</small>
						</div>
					</div>
				</div>
			);
		}
	}
);
