import * as React from "react";
import * as PropTypes from "prop-types";
import { Models } from "./../../@imports";

export namespace ReviewListItemProps {
	export interface IProps extends React.Props<any> {
		entity: Models.IReview;
	}

	export const types: PropTypes.ValidationMap<any/*IProps*/> = {
		// Empty
	}

	export const defaults: IProps = {
		entity: null
	}
}
