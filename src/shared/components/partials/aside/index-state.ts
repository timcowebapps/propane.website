"use strict";

export namespace AsideState {
	export interface IState {
		opened: boolean;
	}
}
