"use strict";

const path = require("path");
const nodeExternals = require("webpack-node-externals");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const pkgConfig = require("./../package.config.js");
const alias = require("./alias.js");

module.exports = {
	target: "node",
	entry: {
		"bundle.server": path.resolve(__dirname, "../", pkgConfig.paths.src.base, "server")
	},
	output: {
		path: path.resolve(__dirname, "../", pkgConfig.paths.dst.base),
		libraryTarget: "commonjs2"
	},
	externals: [nodeExternals()],
	module: {
		rules: [
			{
				test: /\.ts[x]?$/,
				loader: "ts-loader"
			}, {
				test: /\.(scss|css)$/,
				resolve: { extensions: [".scss", ".css"] },
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: "css-loader", options: {
							sourceMap: true,
							importLoaders: 2,
							modules: {
								localIdentName: pkgConfig.devbuild
									? "[path][name]__[local]--[hash:base64:5]"
									: "[hash:base64:5]"
							}
						}
					},
					"sass-loader",
				]
			}, {
				test: /\.(jp[e]?g|png|gif|svg)$/,
				loader: "file-loader?name=img/[name].[ext]"
			}, {
				test: /\.html$/,
				loader: "file-loader?name=[name].[ext]"
			}, {
				test: /\.ico$/,
				loader: "file-loader?name=[name].[ext]"
			}
		]
	},
	resolve: {
		extensions: [".js", ".jsx", ".ts", ".tsx"],
		alias: alias,
		modules: [
			path.resolve(__dirname, "../node_modules")
		]
	}
};
