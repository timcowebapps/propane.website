"use strict";

export namespace Bundles {
	export function getScripts(bundles) {
		return bundles.filter(bundle => bundle.file.endsWith(".js")).map(bundle => {
			return `<script type="text/javascript" src="${bundle.file}"></script>\n`;
		});
	}

	export function getStyles(bundles) {
		return bundles.filter(bundle => bundle.file.endsWith(".css")).map(bundle => {
			return `<link rel="stylesheet" src="${bundle.file}" />\n`;
		});
	}
}
