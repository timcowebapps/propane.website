"use strict";

export interface ISectionProps {
	isCurrSection: boolean;
	onSectionChange: (sectionIdx: number) => void;
}
