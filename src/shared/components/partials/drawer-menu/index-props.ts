"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";

export namespace DrawerMenuProps {
	export interface IProps {
		onCloseMenu: any;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		// Empty
	}

	export const defaults: IProps = {
		onCloseMenu: undefined
	}
}
