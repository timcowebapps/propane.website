"use strict";

import { Models } from "./../@imports";

export namespace ReviewsPageState {
	export interface IState {
		hits: Models.IReview[];
		page: number;
	}
}
