"use strict";

import * as path from "path";
import * as express from "express";

const favicon = require("serve-favicon");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const compression = require("compression");
const vash = require("vash");

export class Server {
	public app: express.Application;

	/**
	 * Создание сервера приложения.
	 *
	 * @class Server
	 * @static
	 * @method bootstrap
	 * @return {Server} Возвращает новый экземпляр класса.
	 */
	public static bootstrap = (): Server => new Server();

	/**
	 * Конструктор класса.
	 * 
	 * @class Server
	 * @public
	 * @constructor
	 */
	public constructor() {
		this.app = express();

		this._config();
		this._mailer();
	}

	/**
	 * Устанавливает конфигурацию приложения.
	 * 
	 * @class Server
	 * @private
	 * @method _config
	 */
	private _config(): void {
		this.app.use(bodyParser.urlencoded({ extended: true }));
		this.app.use(bodyParser.json());
		this.app.use(compression());

		/** Добавляем статические пути. */
		this.app.use(express.static(path.join(process.cwd(), "wwwroot")));
		this.app.use(express.static(path.join(process.cwd(), "wwwroot", "dst")));

		this.app.use("/assets", express.static(path.join(process.cwd(), "wwwroot")));
		this.app.use("/assets/fonts", express.static(path.join(process.cwd(), "wwwroot", "fonts")));
		this.app.use("/assets/icons", express.static(path.join(process.cwd(), "wwwroot", "icons")));
		this.app.use("/assets/images", express.static(path.join(process.cwd(), "wwwroot", "images")));

		this.app.use(favicon(path.join(process.cwd(), "wwwroot", "favicon.ico")));

		/** Устанавливаем шаблонизатор для представления. */
		this.app.set("view engine", "cshtml");
		this.app.set("views", path.join(process.cwd(), "wwwroot"));
		this.app.engine("cshtml", vash.renderFile);
	}

	private _mailer() {
		this.app.post("/api/form", (req, res, next) => {
			let transporter = nodemailer.createTransport({
				service: "gmail",
				host: "smtp.gmail.com",
				port: 465,
				secure: true,
				auth: {
					type: "OAuth2",
					user: "barefootfabler@gmail.com",
					clientId: "710679381868-gb5i91l9rp5qql0e8i9n8d6jfscigs83.apps.googleusercontent.com",
					clientSecret: "ER1zcgv0-0Ez4k6v1Svj3mEN",
					refreshToken: "1/_mnGeNCGi3R0CER7FJ3wa1CqeL40V1N9l3VQvlMJQhg"
				}
			});

			let mailOptions = {
				from: "My <barefootfabler@gmail.com>",
				to: "barefootfabler@gmail.com",
				subject: "Hello",
				html: "<b>Hello world?</b>"
			};

			transporter.sendMail(mailOptions, (error: any, info: any) => {
				if (error)
					return console.log(error);
			});
		});
	}
};
