"use strict";

import * as path from "path";
import * as express from "express";
import { renderToString } from "react-dom/server";
import { getBundles } from "react-loadable/webpack";
import { createMemoryHistory } from "history";
import { configureStore } from "./../shared/configurestore";
import { Server } from "./server";
import { Root } from "./root";
import { Bundles } from "./bundles";

const pkgConfig = require("./../../package.config.js");
const manifest = require("./../../wwwroot/asset-manifest.json");
const stats = require("./../../wwwroot/react-loadable.json");

export default function () {
	const app = Server.bootstrap().app;

	app.get("*.js", (request: express.Request, response: express.Response, next: express.NextFunction) => {
		request.url = request.url + ".gz";
		response.set("Content-Encoding", "gzip");
		response.set("Content-Type", "text/javascript");
		next();
	});

	app.get("*.css", (request: express.Request, response: express.Response, next: express.NextFunction) => {
		request.url = request.url + ".gz";
		response.set("Content-Encoding", "gzip");
		response.set("Content-Type", "text/css");
		next();
	});

	// if (pkgConfig.devbuild) {
	// 	// Empty
	// } else {
	// 	app.get("*", (request: express.Request, response: express.Response, next: express.NextFunction) => {
	// 		if (request.headers["x-forwarded-proto"] != "https")
	// 			response.redirect("https://"+ request.hostname + request.url);
	// 		else
	// 			next();
	// 	});
	// }

	app.get("*", (request: express.Request, response: express.Response) => {
		let memoryHistory = createMemoryHistory();
		let store = configureStore(memoryHistory, {});
		let context = {};
		let modules = [];
		let html = renderToString(Root({ request: request, store: store, context: context, modules: modules }));
		let bundles = getBundles(stats, modules);

		response.status(200).render("index.cshtml", {
			markup: html,
			scripts: Bundles.getScripts(bundles),
			styles: Bundles.getStyles(bundles),
			preloadedState: JSON.stringify(store.getState()).replace(/</g, "\\u003c")
		});
	});

	// https://bbs.ptsecurity.com
	const blackboxScannerHtml = "e7051d38e7580eb569c753c708fb78820649b92bc503939f3e7857d439f07fda.html";
	app.get(`/${blackboxScannerHtml}`, (request: express.Request, response: express.Response) => {
		response.sendFile(path.join(process.cwd(), blackboxScannerHtml));
	});

	return app;
}
