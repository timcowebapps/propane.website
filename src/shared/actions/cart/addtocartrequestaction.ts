"use strict";

import { Action, ActionCreator } from "redux";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { AddToCartActionTypeState } from "./addtocartactiontypestate";

export const addToCartRequestAction: ActionCreator<Action> = (product: any): IAction<any> => {
	return {
		type: AddToCartActionTypeState.toStr(ActionTypeStates.REQUEST),
		payload: {
			item: product
		}
	}
}

export const addToCartRequest = addToCartRequestAction;
