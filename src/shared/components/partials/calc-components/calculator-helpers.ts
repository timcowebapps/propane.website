"use strict";

export namespace CalculatorHelpers {
	/**
	 * Вычисление площади потолка.
	 * 
	 * @param {number} w 
	 * @param {number} h 
	 * @example 2,4 * 3,8 = 9,12 (квадратный метр).
	 */
	export function calculateArea(w: number, h: number): number {
		return Math.round((w * h) * 10) / 10;
	}

	/**
	 * Вычисление периметра.
	 * 
	 * @param {number} w 
	 * @param {number} h 
	 * @example 2,4 * 2 + 3,8 * 2 = 12,4 метра.
	 */
	export function calculatePerimeter(w: number, h: number): number {
		w = w * 1;
		h = h * 1;

		return Math.ceil((w + h) * 2);
	}

	export function getAreaFixed(value: number, fractionDigits: number = 0) {
		return Number((Math.round(value * 10) / 10).toFixed(fractionDigits));
	}
}
