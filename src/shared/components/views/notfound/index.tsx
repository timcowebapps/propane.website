"use strict";

import * as React from "react";

export default class NotFound extends React.Component<any, any> {
	/**
	 * Конструктор класса.
	 * 
	 * @class NotFound
	 * @public
	 * @constructor
	 * @param {any} props Свойства компонента.
	 */
	public constructor(props?: any) {
		super(props);
	}

	/**
	 * Компонент примонтировался.
	 * В данный момент у нас есть возможность использовать refs.
	 * 
	 * @class NotFound
	 * @public
	 */
	public componentDidMount() {
		/** Устанавливаем заголовок документа */
		document.title = "Страница не найдена";
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class NotFound
	 * @public
	 */
	public render(): JSX.Element {
		return (
			<React.Fragment>
				<div>
					<h2>404</h2>
					<h3>Page not found</h3>
				</div>
			</React.Fragment>
		);
	}
};
