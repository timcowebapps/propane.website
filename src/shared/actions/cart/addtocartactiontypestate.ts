"use strict";

import * as _ from "lodash";
import { EnumTransform } from "@timcowebapps/react.utils";
import { ActionTypes } from "./../actiontypes";
import { ActionTypeStates } from "./../actiontypestates";
import { Constants } from "./../actionconstants";

export namespace AddToCartActionTypeState {
	export const toStr = (key: any): string => Constants.REDUX_STORE + 
		_.join([
			EnumTransform.toStr(ActionTypes, ActionTypes.ADD), "TO_CART",
			EnumTransform.toStr(ActionTypeStates, key)
		], "_");
}
