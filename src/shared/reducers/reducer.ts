"use strict";

import * as _ from "lodash";
import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { IReducerStoreState } from "./reducerstorestate";
import { productReducer } from "./product/productreducer";
import { cartReducer } from "./cart/cartreducer";
import { calculatorReducer } from "./calculator/calculatorreducer";

export default (history: any) => combineReducers<IReducerStoreState>({
	router: connectRouter(history),
	calculator: calculatorReducer,
	product: productReducer,
	cart: cartReducer
});
