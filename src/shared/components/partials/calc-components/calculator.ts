"use strict";

import { EnumHelpers } from "@timcowebapps/react.utils";
import * as Models from "./../../../stores/models/@export-models";
import { CalculatorHelpers } from "./calculator-helpers";

export class Calculator {
	//#region Приватные переменные

	private _payload: Models.ICostServicePayload = null;
	private _pricelist: Models.ICost = null;

	//#endregion

	//#region Gets/Sets

	get payload(): Models.ICostServicePayload { return this._payload }
	set payload(payload: Models.ICostServicePayload) { this._payload = payload }

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Calculator
	 * @public
	 * @constructor
	 * @param {Models.ICost} pricelist Фиксированные цены.
	 */
	public constructor(pricelist: Models.ICost) {
		this._pricelist = pricelist;
	}

	/**
	 * Цена полотна за 1м2.
	 * 
	 * @class Calculator
	 * @public
	 * @param {Models.ICostServicePayload} payload Данные.
	 */
	public getCanvas(payload: Models.ICostServicePayload) {
		return EnumHelpers.compareWithNum(Models.MaterialTypes, Models.MaterialTypes.Cloth, payload.selected_material)
			? this._pricelist.canvas.cloth
			: this._pricelist.canvas.pvh;
	}

	/**
	 * Цена полотна.
	 * 
	 * @class Calculator
	 * @public
	 * @param {Models.ICostServicePayload} payload Данные.
	 */
	public getFullCanvas(payload: Models.ICostServicePayload) {
		return this.getCanvas(payload) * CalculatorHelpers.getAreaFixed(payload.area);
	}

	/**
	 * Цена полотна за 1м2 с работой.
	 * 
	 * @class Calculator
	 * @public
	 * @param {Models.ICostServicePayload} payload Данные.
	 */
	public getCanvasWithWork(payload: Models.ICostServicePayload) {
		return this.getCanvas(payload) + this._pricelist.canvas.montage;
	}

	/**
	 * Цена полотна с работой.
	 * 
	 * @class Calculator
	 * @public
	 * @param {Models.ICostServicePayload} payload Данные.
	 */
	public getFullCanvasWithWork(payload: Models.ICostServicePayload) {
		return this.getFullCanvas(payload) + this._pricelist.canvas.montage * CalculatorHelpers.getAreaFixed(payload.area);
	}

	public getCostCorners(data: Models.ICostServicePayload) {
		return data.number_corners * this._pricelist.corner;
	}

	public getCostPipes(data: Models.ICostServicePayload) {
		return data.number_pipes * this._pricelist.pipe;
	}
}
