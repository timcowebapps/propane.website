"use strict";

import * as _ from "lodash";
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as PropTypes from "prop-types";
import { Guid } from "@timcowebapps/react.system";
import { Methodology } from "@timcowebapps/react.style";
import { Data } from "@timcowebapps/react.componentmodel";
import { EnumHelpers } from "@timcowebapps/react.utils";
import { Checkbox, Price } from "@timcowebapps/react.toolkit";
import * as Models from "./../../../../stores/models/@export-models";
import { CustomSlider } from "./../custom-slider/index";
import { CustomTabPaneProps } from "./index-props";
import { CustomTabPaneState } from "./index-state";

const styles = require("./../../../views/calc/index.scss");

export default class CustomTabPane extends React.Component<CustomTabPaneProps.IProps, CustomTabPaneState.IState> {
	//#region Статические переменные

	public static displayName: string = "CustomTabPane";
	public static propTypes: PropTypes.ValidationMap<CustomTabPaneProps.IProps> = CustomTabPaneProps.types;
	public static defaultProps: CustomTabPaneProps.IProps = CustomTabPaneProps.defaults;

	//#endregion

	//#region Приватные переменные

	private _guids = [];

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class CustomTabPane
	 * @private
	 */
	private _getInitialState(): CustomTabPaneState.IState {
		return {
			data: this.props.initialValues
		}
	}

	//#region Events

	private _handleRoomChange(event: any, value: any): void {
		const data = this.state.data;
		data.selected_room = value.value;

		this.setState({
			data: data
		}, _.invoke(this.props, "onChangeHandler", data));
	}

	private _handleMaterialChange(event: any, value: any): void {
		const data = this.state.data;
		data.selected_material = value.value;

		this.setState({
			data: data
		}, _.invoke(this.props, "onChangeHandler", data));
	}

	private _handleAreaChange(value: number): void {
		const data = this.state.data;
		data.area = value;

		this.setState({
			data: data
		}, _.invoke(this.props, "onChangeHandler", data));
	}

	private _handleAnglesChange(value: number): void {
		const data = this.state.data;
		data.number_corners = value;

		this.setState({
			data: data
		}, _.invoke(this.props, "onChangeHandler", data));
	}

	private _handlePipesChange(value: number): void {
		const data = this.state.data;
		data.number_pipes = value;

		this.setState({
			data: data
		}, _.invoke(this.props, "onChangeHandler", data));
	}

	private _handleLustersChange(value: number): void {
		const data = this.state.data;
		data.number_lusters = value;

		this.setState({
			data: data
		}, _.invoke(this.props, "onChangeHandler", data));
	}

	private _handleLampsChange(value: number): void {
		const data = this.state.data;
		data.number_lamps = value;

		this.setState({
			data: data
		}, _.invoke(this.props, "onChangeHandler", data));
	}

	//#endregion

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class CustomTabPane
	 * @public
	 * @constructor
	 * @param {CustomTabPaneProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: CustomTabPaneProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		//#region Bindings

		this._handleRoomChange = this._handleRoomChange.bind(this);
		this._handleMaterialChange = this._handleMaterialChange.bind(this);
		this._handleAreaChange = this._handleAreaChange.bind(this);
		this._handleAnglesChange = this._handleAnglesChange.bind(this);
		this._handlePipesChange = this._handlePipesChange.bind(this);
		this._handleLustersChange = this._handleLustersChange.bind(this);
		this._handleLampsChange = this._handleLampsChange.bind(this);

		//#endregion

		this._guids = [
			Guid.newGuid(), /** Площадь */
			Guid.newGuid(), /** Углы */
			Guid.newGuid(), /** Трубы */
			Guid.newGuid(), /** Люстры */
			Guid.newGuid()  /** Светильники */
		];
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class CustomTabPane
	 * @public
	 */
	public render(): JSX.Element {
		/** Цена полотна 1м2. */
		const canvas = EnumHelpers.compareWithNum(Models.MaterialTypes, Models.MaterialTypes.Cloth, this.state.data.selected_material) ? this.props.pricelist.canvas.cloth : this.props.pricelist.canvas.pvh;
		/** Цена полотна 1м2 с работой. */
		const canvasWithWork = canvas + this.props.pricelist.canvas.montage;

		/** Площадь потолка. */
		const ceilingArea = Number((Math.round(this.state.data.area * 10) / 10).toFixed(0));

		/** Отдельно полотно. */
		const separatelyCanvas = ceilingArea * canvas;
		/** Отдельно монтаж. */
		const separatelyMontage = ceilingArea * this.props.pricelist.canvas.montage;

		const corners = this.state.data.number_corners * this.props.pricelist.corner;
		const pipes = this.state.data.number_pipes * this.props.pricelist.pipe;
		const lusters = this.state.data.number_lusters * this.props.pricelist.luster;
		const lamps = this.state.data.number_lamps * this.props.pricelist.lamp;

		return (
			<React.Fragment>
				<div className={styles["calc-step1"]}>
					<div className={Methodology.Bem.Entities.block(styles, "calc-step1").element("header").toStr()}>Тип помещения</div>
					<div className={Methodology.Bem.Entities.block(styles, "calc-step1").element("content").toStr()}>
						<ul className={styles["list-unstyled"]}>
							{Object.keys(Models.RoomTypes).filter(key => Models.RoomTypes[key as any]).map((val: string, index: number) => {
								return <li key={index} className={styles["list-inline-item"]}>
									<Checkbox {...{
										uid: Math.random().toString(),
										requirements: {
											name: "room",
											value: val,
											checked: this.state.data.selected_room === val,
											onChange: this._handleRoomChange,
											viewStyle: { stylesheet: styles, bem: { block: "checkbox" } }
										},
										items: [{
											type: Data.Schema.ComponentTypes.Input,
											requirements: { name: "room" }
										}, {
											type: Data.Schema.ComponentTypes.Icon,
											requirements: {
												width: "1em", height: "1em",
												viewBox: { x: 0, y: 0, width: 13, height: 10 },
												preserveAspectRatio: "xMidYMid meet",
											},
											items: [{
												type: Data.Schema.ComponentTypes.Node,
												requirements: {
													content: React.createElement("g", { key: Math.random() },
														<polygon key={Math.random()} xmlns="http://www.w3.org/2000/svg" fill="#92c9c2" points="11,0.2 4.5,6.7 2,4.2 0.5,5.7 4.5,9.8 12.5,1.7" />
													)
												}
											}]
										}, {
											type: Data.Schema.ComponentTypes.Label,
											requirements: { text: Models.RoomTypes[val] }
										}]
									}} />
								</li>
							})}
						</ul>
					</div>
				</div>

				<div className={styles["calc-step2"]}>
					<h6 className={Methodology.Bem.Entities.block(styles, "calc-step2").element("header").toStr()}>Тип полотна</h6>
					<div className={Methodology.Bem.Entities.block(styles, "calc-step2").element("content").toStr()}>
						<ul className={styles["list-unstyled"]}>
							<li className={styles["list-inline-item"]}>
								<Checkbox key={Guid.newGuid()} {...{
									uid: Math.random().toString(),
									requirements: {
										name: "material",
										value: Models.MaterialTypes.PVH,
										checked: EnumHelpers.compareWithNum(Models.MaterialTypes, Models.MaterialTypes.PVH, this.state.data.selected_material),
										onChange: this._handleMaterialChange,
										viewStyle: { stylesheet: styles, bem: { block: "checkbox" } }
									},
									items: [{
										type: Data.Schema.ComponentTypes.Input,
										requirements: { name: "material" }
									}, {
										type: Data.Schema.ComponentTypes.Label,
										requirements: { text: "ПВХ" }
									}]
								}} />
							</li>
							<li className={styles["list-inline-item"]}>
								<Checkbox key={Guid.newGuid()} {...{
									uid: Math.random.toString(),
									requirements: {
										value: Models.MaterialTypes.Cloth,
										checked: EnumHelpers.compareWithNum(Models.MaterialTypes, Models.MaterialTypes.Cloth, this.state.data.selected_material),
										onChange: this._handleMaterialChange,
										viewStyle: { stylesheet: styles, bem: { block: "checkbox" } }
									},
									items: [{
										type: Data.Schema.ComponentTypes.Input,
										requirements: { name: "material" }
									}, {
										type: Data.Schema.ComponentTypes.Label,
										requirements: { text: "Ткань" }
									}]
								}} />
							</li>
						</ul>
					</div>
				</div>

				<div className={styles["calc-step3"]}>
					<h6 className={Methodology.Bem.Entities.block(styles, "calc-step3").element("header").toStr()}>Параметры помещения</h6>
					<div className={Methodology.Bem.Entities.block(styles, "calc-step3").element("content").toStr()}>

						<CustomSlider {...{
							uid: "area_id",
							requirements: {
								htmlId: "area_id",
								name: "area",
								min: 0, max: 50, step: 1,
								value: this.state.data.area,
								unit: <React.Fragment>м<sup>2</sup></React.Fragment>,
								onChanged: this._handleAreaChange
							},
							items: [{
								uid: "label",
								requirements: { text: "Площадь помещения" }
							}, {
								uid: "input",
								requirements: {}
							}, {
								uid: "sumup",
								requirements: {
									piece: "Отдельно полотно",
									total: <React.Fragment>от&nbsp;
										<Price {...{
											requirements: {
												tag: "span",
												amount: separatelyCanvas, currency: "руб.",
												viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["sale"] } }
											}
										}} />
									</React.Fragment>
								}
							}]
						}} />

						<CustomSlider {...{
							uid: "corners_id",
							requirements: {
								htmlId: "corners_id",
								name: "corners",
								min: 3, max: 15, step: 1,
								value: this.state.data.number_corners,
								unit: <React.Fragment>шт.</React.Fragment>,
								onChanged: this._handleAnglesChange
							},
							items: [{
								uid: "label",
								requirements: { text: "Количество углов" }
							}, {
								uid: "input",
								requirements: {}
							}, {
								uid: "sumup",
								requirements: {
									piece: <span>{this.props.pricelist.corner} руб. за 1 шт.</span>,
									total: <React.Fragment>от&nbsp;
										<Price {...{
											requirements: {
												tag: "span",
												amount: corners, currency: "руб.",
												viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["sale"] } }
											}
										}} />
									</React.Fragment>
								}
							}]
						}} />

						<CustomSlider {...{
							uid: "pipes_id",
							requirements: {
								htmlId: "pipes_id",
								name: "pipes",
								min: 0, max: 10, step: 1,
								value: this.state.data.number_pipes,
								unit: <React.Fragment>шт.</React.Fragment>,
								onChanged: this._handlePipesChange
							},
							items: [{
								uid: "label",
								requirements: { text: "Количество труб" /* Количество труб, уходящих в потолок */ }
							}, {
								uid: "input",
								requirements: {}
							}, {
								uid: "sumup",
								requirements: {
									piece: <span>{this.props.pricelist.pipe} руб. за 1 шт.</span>,
									total: <React.Fragment>от&nbsp;
										<Price {...{
											requirements: {
												tag: "span",
												amount: pipes, currency: "руб.",
												viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["sale"] } }
											}
										}} />
									</React.Fragment>
								}
							}]
						}} />
					</div>
				</div>

				<div className={styles["calc-step4"]}>
					<h6 className={Methodology.Bem.Entities.block(styles, "calc-step4").element("header").toStr()}>Освещение и аксессуары</h6>
					<div className={Methodology.Bem.Entities.block(styles, "calc-step4").element("content").toStr()}>
						<CustomSlider {...{
							uid: "lusters_id",
							requirements: {
								htmlId: "lusters_id",
								name: "lusters",
								min: 0, max: 10, step: 1,
								value: this.state.data.number_lusters,
								unit: <React.Fragment>шт.</React.Fragment>,
								onChanged: this._handleLustersChange
							},
							items: [{
								uid: "label",
								requirements: { text: "Количество люстр" }
							}, {
								uid: "input",
								requirements: {}
							}, {
								uid: "sumup",
								requirements: {
									piece: <span>{this.props.pricelist.luster} руб. за 1 шт.</span>,
									total: <React.Fragment>от&nbsp;
										<Price {...{
											requirements: {
												tag: "span",
												amount: lusters, currency: "руб.",
												viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["sale"] } }
											}
										}} />
									</React.Fragment>
								}
							}]
						}} />

						<CustomSlider {...{
							uid: "lamps_id",
							requirements: {
								htmlId: "lamps_id",
								name: "lamps",
								min: 0, max: 50, step: 1,
								value: this.state.data.number_lamps,
								unit: <React.Fragment>шт.</React.Fragment>,
								onChanged: this._handleLampsChange
							},
							items: [{
								uid: "label",
								requirements: { text: "Количество светильников" }
							}, {
								uid: "input",
								requirements: {}
							}, {
								uid: "sumup",
								requirements: {
									piece: <span>{this.props.pricelist.lamp} руб. за 1 шт.</span>,
									total: <React.Fragment>от&nbsp;
										<Price {...{
											requirements: {
												tag: "span",
												amount: lamps, currency: "руб.",
												viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["sale"] } }
											}
										}} />
									</React.Fragment>
								}
							}]
						}} />
					</div>
				</div>
			</React.Fragment>
		);
	}
}
