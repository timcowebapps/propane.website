"use strict";

import * as Loadable from "react-loadable";
import AsyncLoader from "../../partials/asyncloader";

const loadableOptions: LoadableExport.OptionsWithoutRender<any> = {
	loader: () => import(/* webpackChunkName: "NotFound" */"./index"),
	loading: AsyncLoader
}

export default Loadable(loadableOptions);
