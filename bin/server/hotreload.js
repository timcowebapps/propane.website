"use strict";

const webpack = require("webpack");
const WebpackDevServer = require("webpack-dev-server");
const pkgConfig = require("./../../package.config.js");
const configClient = require("./../../webpack/webpack.config.dev.js");
const configServer = require("./../../webpack/webpack.config.server.dev.js");

let devServer = new WebpackDevServer(webpack(configClient), {
	contentBase: pkgConfig.dev.contentPath,
	publicPath: pkgConfig.dev.publicPath,
	hot: true,
	inline: true,
	host: pkgConfig.dev.host,
	port: pkgConfig.dev.port,
	overlay: true,
	compress: true,
	stats: {
		chunk: false,
		chunkModules: false,
		modules: false,
		source: false,
		chunkOrigins: false
	},
	open: false,
	historyApiFallback: true,
	disableHostCheck: true,
	watchContentBase: true,
	watchOptions: { ignored: /node_modules/ },
	headers: { "Access-Control-Allow-Origin": "*" }
}).listen(pkgConfig.dev.port, pkgConfig.dev.host, (err) => {
	if (err)
		console.error(err);

	console.log("Webpack server launched with at localhost:%d (Hot Module Replacement [HMR] enabled)", pkgConfig.dev.port);
});

webpack(configServer).watch({}, (err, stats) => {
	if (err)
		return console.error(err.message);
});
