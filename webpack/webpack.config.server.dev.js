"use strict";

const webpack = require("webpack");
const merge = require("webpack-merge");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const pkg = require("./../package.json");
const pkgConfig = require("./../package.config.js");

module.exports = merge(require("./webpack.config.server.base.js"), {
	name: "dev.server",
	mode: "development",
	devtool: "cheap-module-source-map",
	externals: [
		/^[a-z\-0-9]+$/, {
			"react-dom/server": true
		}
	],
	output: {
		filename: "[name].js",
		publicPath: pkgConfig.dev.publicPath
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: "[name].css",
			chunkFilename: "[name].chunk.css"
		}),
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: JSON.stringify("development"),
				BROWSER: JSON.stringify(false),
				VERSION: JSON.stringify(pkg.version)
			}
		}),
		new webpack.ProvidePlugin({
			"React": "react",
			"ReactDOM": "react-dom"
		})
	]
});
