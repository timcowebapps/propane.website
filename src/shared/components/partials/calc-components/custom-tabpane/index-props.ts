"use strict";

import * as PropTypes from "prop-types";
import * as Models from "./../../../../stores/models/@export-models";

export namespace CustomTabPaneProps {
	export interface IProps {
		pricelist: Models.ICost;
		initialValues: Models.ICostServicePayload;
		onChangeHandler: (change: Models.ICostServicePayload) => void;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		pricelist: PropTypes.any,
		initialValues: PropTypes.any,
		onChangeHandler: PropTypes.func
	}

	export const defaults: IProps = {
		initialValues: null,
		pricelist: null,
		onChangeHandler: undefined
	}
}
