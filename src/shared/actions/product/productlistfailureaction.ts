"use strict";

import { Action, ActionCreator } from "redux";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { ProductListActionTypeState } from "./productlistactiontypestate";

export const productListFailureAction: ActionCreator<Action> = (error: any): IAction<any> => {
	return {
		type: ProductListActionTypeState.toStr(ActionTypeStates.FAILURE),
		payload: {
			error: error
		}
	};
}

export const productListFailure = productListFailureAction;
