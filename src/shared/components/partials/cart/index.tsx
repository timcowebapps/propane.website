"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { connect } from "react-redux";
import { Methodology } from "@timcowebapps/react.style";
import { Data } from "@timcowebapps/react.componentmodel";
import { HtmlTagTypes, HtmlInputTypes } from "@timcowebapps/react.utils";
import { Hyperlink, Button, NumInput, Price } from "@timcowebapps/react.toolkit";
import * as Models from "./../../../stores/models/@export-models";
import { CartProps } from "./index-props";
import { CartState } from "./index-state";
import EmptyCart from "./empty";

const styles = require("./index.scss");
const inputStyles = require("./../../ui/input/variants.scss");
const buttonStyles = require("./../../ui/button/variants.scss");

export default connect(CartProps.mapStateToProps, CartProps.mapDispatchToProps)(
	class Cart extends React.Component<CartProps.IProps, CartState.IState> {
		//#region Статические переменные

		public static displayName: string = "Cart";
		public static propTypes: PropTypes.ValidationMap<CartProps.IProps> = CartProps.types;
		public static defaultProps: CartProps.IProps = CartProps.defaults;

		//#endregion

		//#region Приватные методы

		/**
		 * Начальное состояние свойств по умолчанию.
		 * 
		 * @class Cart
		 * @private
		 */
		private _getInitialState(): CartState.IState {
			return {
				quantity: 0
			}
		}

		private _handleQuantityChange = (event: React.FormEvent<HTMLInputElement>, value: string): void => {
			this.setState({ quantity: parseInt(value) });
		}

		//#endregion

		/**
		 * Конструктор класса.
		 * 
		 * @class Cart
		 * @public
		 * @constructor
		 * @param {CartProps.IProps} props Свойства компонента.
		 */
		public constructor(props?: CartProps.IProps) {
			super(props);

			this.state = this._getInitialState();

			//#region Bindings

			this._handleQuantityChange = this._handleQuantityChange.bind(this);

			//#endregion
		}

		/**
		 * Отрисовывает компонент.
		 * 
		 * @class Cart
		 * @public
		 */
		public render(): JSX.Element {
			return <React.Fragment>
				<h4>Моя корзина покупок</h4>
				{(this.props.items.length <= 0)
					? <EmptyCart {...{ requirements: { viewStyle: { stylesheet: styles } } }} />
					: <p className="cart-total">Товаров:&nbsp;{this.props.items.length}&nbsp;(<Price {...{
						requirements: {
							tag: "span",
							amount: 0.00, currency: "руб.",
							viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["sale"] } }
						}
					}} />)</p>
				}

				{this.props.items.map((item: Models.IProduct, idx: number) => {
					return (
						<div key={idx} className={styles["product-item"]}>
							<div className={Methodology.Bem.Entities.block(styles, "product-item").element("img").toStr()}>
								<img itemProp="image" src={item.thumb} />
							</div>
							<div className={Methodology.Bem.Entities.block(styles, "product-item").element("info").toStr()}>
								<h4>
									<Hyperlink {...{
										requirements: {
											htmlTag: HtmlTagTypes.A,
											to: "/",
											viewStyle: { stylesheet: styles, bem: { block: "btn", modifiers: ["title-link"] } }
										},
										items: [{
											type: Data.Schema.ComponentTypes.Node,
											requirements: { content: item.brand + " " + item.model }
										}]
									}} />
								</h4>
								<span className="price">{item.price.sale}</span>
							</div>
							<div className={Methodology.Bem.Entities.block(styles, "product-item").element("quantity").toStr()}>

								<NumInput {...{
									requirements: {
										viewStyle: { stylesheet: inputStyles, bem: { block: "num-input" } }
									},
									items: [{
										type: Data.Schema.ComponentTypes.Input,
										requirements: {
											htmlType: HtmlInputTypes.Number,
											name: "quantity",
											value: this.state.quantity.toString(),
											validators: [],
											onValueChange: this._handleQuantityChange,
											viewStyle: { stylesheet: inputStyles, bem: { block: "num-input", element: "input" } }
										}
									}, {
										type: Data.Schema.ComponentTypes.Spinner,
										items: [{
											type: Data.Schema.ComponentTypes.SpinButton,
											requirements: {
												htmlTag: HtmlTagTypes.Button,
												dataAttribs: {
													quantity: "decrement"
												},
												viewStyle: { stylesheet: buttonStyles, bem: { block: "num-input", element: "btn", modifiers: ["primary", "decrement"] } }
											},
											items: [{
												type: Data.Schema.ComponentTypes.Node,
												requirements: {
													content: "-"
												}
											}]
										}, {
											type: Data.Schema.ComponentTypes.SpinButton,
											requirements: {
												htmlTag: HtmlTagTypes.Button,
												dataAttribs: {
													quantity: "increment"
												},
												viewStyle: { stylesheet: buttonStyles, bem: { block: "num-input", element: "btn", modifiers: ["primary", "increment"] } }
											},
											items: [{
												type: Data.Schema.ComponentTypes.Node,
												requirements: {
													content: "+"
												}
											}]
										}]
									}]
								}} />

							</div>
							<div className={Methodology.Bem.Entities.block(styles, "product-item").element("unit").toStr()}>шт</div>
							<Button {...{
								requirements: {
									htmlTag: HtmlTagTypes.Button,
									onClick: (event: React.MouseEvent<HTMLButtonElement>) => this.props.actionCreators.removeFromCart(item.id),
									viewStyle: { stylesheet: styles, bem: { block: "btn-trash" } }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Icon,
									requirements: {
										width: "1em", height: "1em",
										viewBox: { x: 0, y: 0, width: 24, height: 24 },
										preserveAspectRatio: "xMidYMid meet",
										viewStyle: { stylesheet: styles, bem: { block: "icon" } }
									},
									items: [
										{ requirements: { geometry: "M 3,5 A 1.0001,1.0001 0 1 0 3,7 H 5 21 A 1.0001,1.0001 0 1 0 21,5 H 5 Z" } },
										{ requirements: { geometry: "M 10,1 C 8.3549904,1 7,2.3549904 7,4 V 6 A 1.0001,1.0001 0 1 0 9,6 V 4 C 9,3.4358706 9.4358706,3 10,3 h 4 c 0.564129,0 1,0.4358706 1,1 v 2 a 1.0001,1.0001 0 1 0 2,0 V 4 C 17,2.3549904 15.64501,1 14,1 Z M 4.984375,4.9863281 A 1.0001,1.0001 0 0 0 4,6 v 14 c 0,1.64501 1.3549904,3 3,3 h 10 c 1.64501,0 3,-1.35499 3,-3 V 6 a 1.0001,1.0001 0 1 0 -2,0 v 14 c 0,0.564129 -0.435871,1 -1,1 H 7 C 6.4358706,21 6,20.564129 6,20 V 6 A 1.0001,1.0001 0 0 0 4.984375,4.9863281 Z" } }
									]
								}]
							}} />
						</div>
					);
				})}
			</React.Fragment>
		}
	}
);
