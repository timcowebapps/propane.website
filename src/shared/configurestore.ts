"use strict";

import { routerMiddleware } from "react-router-redux";
import { applyMiddleware, createStore, compose } from "redux";
import { createLogger } from "redux-logger";
import createSagaMiddleware from "redux-saga";
import createRootReducer from "./reducers/reducer";
import shopSage from "./sagas";

export function configureStore(history: any, initialState?: any) {
	const sagaMiddleware = createSagaMiddleware();
	const middlewares = [
		sagaMiddleware,
		routerMiddleware(history)
	];

	if (typeof window !== "undefined" && process.env.NODE_ENV === "development") {
		middlewares.push(createLogger({
			level: "info",
			collapsed: true
		}));
	}

	const store = compose(
		applyMiddleware(...middlewares)
	)(createStore)(createRootReducer(history), initialState);

	sagaMiddleware.run(shopSage);

	return store;
}
