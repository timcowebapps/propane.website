"use strict";

export enum ActionTypes {
	RECEIVE,
	ADD,
	REMOVE,
	CHANGE
}
