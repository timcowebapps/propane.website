"use strict";

import * as _ from "lodash";
import * as React from "react";
import { Link } from "react-router-dom";
import * as PropTypes from "prop-types";
import { Methodology } from "@timcowebapps/react.style";
import { Hyperlink, Menu } from "@timcowebapps/react.toolkit";
import { CN, OrientTypes } from "@timcowebapps/react.utils";
import { RouteConstants } from "./../../../routeconstants";
import Cart from "./../cart/index";
import { DrawerMenuProps } from "./index-props";
import { DrawerMenuState } from "./index-state";

const styles: any = require("./index.scss");

export default class DrawerMenu extends React.Component<DrawerMenuProps.IProps, DrawerMenuState.IState> {
	//#region Статические переменные

	public static displayName: string = "DrawerMenu";
	public static propTypes: PropTypes.ValidationMap<DrawerMenuProps.IProps> = DrawerMenuProps.types;
	public static defaultProps: DrawerMenuProps.IProps = DrawerMenuProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class DrawerMenu
	 * @private
	 */
	private _getInitialState(): DrawerMenuState.IState {
		return {
			// Empty
		}
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class DrawerMenu
	 * @public
	 * @constructor
	 * @param {DrawerMenuProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: DrawerMenuProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class DrawerMenu
	 * @public
	 */
	public render(): JSX.Element {
		return <React.Fragment>
			<div className={styles["container"]}>
				<div className={CN.many(
					Methodology.Bem.Entities.block(styles, "row").element().toStr(),
					Methodology.Bem.Entities.block(styles, "spacing-above").element().modifiers(["lg"])
				)}>
					<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
						<Menu {...{
							requirements: {
								orient: OrientTypes.Horizontal,
								viewStyle: { stylesheet: styles, bem: { block: "menu" } }
							},
							items: [{
								uid: "0",
								requirements: { content: <Link to={RouteConstants.Home} onClick={this.props.onCloseMenu}>Главная</Link> }
							}, {
								uid: "1",
								requirements: { content: <Link to={RouteConstants.Calc} onClick={this.props.onCloseMenu}>Калькулятор</Link> }
							}, {
								uid: "2",
								requirements: { content: <Link to={RouteConstants.Shop} onClick={this.props.onCloseMenu}>Освещение</Link> }
							}, {
								uid: "3",
								requirements: { content: <Link to={RouteConstants.Reviews} onClick={this.props.onCloseMenu}>Обзоры</Link> }
							}]
						}} />
					</div>
				</div>

				<div className={CN.many(
					Methodology.Bem.Entities.block(styles, "row").element().toStr(),
					Methodology.Bem.Entities.block(styles, "spacing-above").element().modifiers(["lg"]),
					Methodology.Bem.Entities.block(styles, "spacing-below").element().modifiers(["lg"])
				)} itemScope itemType="http://schema.org/Organization">
					<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
						<meta itemProp="name" content="Propane" />
						<meta itemProp="description" content="Описание деятельности" />
						<link itemProp="url" href="https://propane-spb.herokuapp.com" />
						<link itemProp="logo" href="https://propane-spb.herokuapp.com/assets/imgs/logo.png" />

						<div itemScope itemType="http://schema.org/ProfessionalService">
							<meta itemProp="name" content="Propane" />
							<meta itemProp="description" content="Описание деятельности" />
							<meta itemProp="priceRange" content="$$" />

							{/* Микроданные адреса (страна, регион, город, адрес, почтовый индекс) */}
							<span itemProp="address" itemScope itemType="http://schema.org/PostalAddress">
								<meta itemProp="addressCountry" content="Россия" />
								<meta itemProp="addressRegion" content="Ленинградская область" />
								<meta itemProp="addressLocality" content="Санкт-Петербург" />
								<meta itemProp="streetAddress" content="Антонова-овсеенко" />
								<meta itemProp="postalCode" content="193168" />
							</span>

							{/* Микроданные логотипа (ссылка на изображение, ширина, высота) */}
							<span itemProp="image" itemScope itemType="http://schema.org/ImageObject">
								<meta itemProp="url" content="https://propane-spb.herokuapp.com/assets/imgs/logo.png" />
								<meta itemProp="width" content="300" />
								<meta itemProp="height" content="300" />
							</span>

							{/* Микроданные рейтинга */}
							<span itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
								<meta itemProp="ratingValue" content="88" />
								<meta itemProp="bestRating" content="100" />
								<meta itemProp="reviewCount" content="43" />
							</span>

							{/* Номера телефонов / Микроданные */}
							<div className={styles["head_contact"]}>
								<h2 className={Methodology.Bem.Entities.block(styles, "text").element().modifiers(["center"])}>С нами легко связаться!</h2>
								<p className={Methodology.Bem.Entities.block(styles, "text").element().modifiers(["center"])}>Вы всегда можете связаться с нами по email, телефону или с помощью контактной формы.</p>
								<div className={Methodology.Bem.Entities.block(styles, "head_contact").element("feedback").modifiers(["phone"])}>
									<a className={Methodology.Bem.Entities.block(styles, _.join(["head_contact", "feedback"], "__")).element("inform").toStr()} href="tel:+79526665222" title="+7 952 666-52-22"><span itemProp="telephone">+7 (952) 666-52-22</span></a>
									<a className={Methodology.Bem.Entities.block(styles, _.join(["head_contact", "feedback"], "__")).element("inform").toStr()} href="tel:+79217466463" title="+7 921 746-64-63"><span itemProp="telephone">+7 (921) 746-64-63</span></a>
								</div>
							</div>

							{/* Режим работы / Микроданные */}
							<div className={styles["head_schedule"]}>
								<div className={Methodology.Bem.Entities.block(styles, "head_schedule").element("time").toStr()}>
									<h4>Режим работы</h4>
									<p className={styles["opening-hours"]}>Пн-Пт:&nbsp;<span>с 9:00 до 21:00</span></p>
									{["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"].map((value: string, idx: number) => {
										return (<span key={idx} itemProp="openingHoursSpecification" itemScope itemType="http://schema.org/OpeningHoursSpecification">
											<link itemProp="dayOfWeek" href={"http://schema.org/" + value} />
											<meta itemProp="opens" content="9:00" />
											<meta itemProp="closes" content="21:00" />
										</span>)
									})}
									<p className={styles["opening-hours"]}>Сб-Вс:&nbsp;<span>Выходной</span></p>
								</div>
							</div>
						</div>

						<Cart />
					</div>
				</div>
			</div>
		</React.Fragment>
	}
}
