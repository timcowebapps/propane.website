import * as React from "react";
import * as PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { IAction } from "./../../../../actions/action";
import { Actions } from "./../../../../sagas/@exports";

export namespace ProductListItemProps {
	export interface IOwnProps extends React.Props<any> {
		// Empty
	}

	export interface IStateProps {
		// Empty
	}

	export interface IDispatchProps {
		actionCreators: {
			addToCart?: (product: any) => IAction<any>;
		}
	}

	export type IProps = IOwnProps & IStateProps & IDispatchProps;

	export const mapStateToProps = (state: any, ownProp?: any): IStateProps => ({
		// Empty
	});

	export const mapDispatchToProps = (dispatch: any): IDispatchProps => ({
		actionCreators: bindActionCreators({
			addToCart: Actions.addToCartRequest
		}, dispatch)
	});

	export const types: PropTypes.ValidationMap<any/*IProps*/> = {
		uid: PropTypes.string,
		requirements: PropTypes.shape({
			payload: PropTypes.shape({
				id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
				thumb: PropTypes.string
			})
		}),
		actionCreators: PropTypes.shape({
			addToCart: PropTypes.func
		})
	}

	export const defaults: IProps = {
		actionCreators: {
			addToCart: undefined
		}
	}
}
