"use strict";

import { IProduct } from "./../../stores/models/product/product";

export interface IProductReducerState {
	loaded: boolean;
	items: Array<IProduct>;
}
