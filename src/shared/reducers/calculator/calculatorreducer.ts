"use strict";

import { IAction, ActionTypeStates, Actions } from "./../../sagas/@exports";
import { CostServiceTabset } from "./../../stores/models/calc/cost-service";

export function calculatorReducer(state = CostServiceTabset.defaults, action: IAction<any>) {
	switch (action.type) {

		//#region Изменение данных

		case Actions.ChangeDataActionTypeState.toStr(ActionTypeStates.REQUEST):
		case Actions.ChangeDataActionTypeState.toStr(ActionTypeStates.FAILURE):
			return state;

		case Actions.ChangeDataActionTypeState.toStr(ActionTypeStates.SUCCESS):
			const updatedItems = state.map(item => {
				if (item.uid === action.payload.uid)
					return { ...item, ...action.payload }

				return item;
			});

			return updatedItems;

		//#endregion

		default:
			return state;
	}
}
