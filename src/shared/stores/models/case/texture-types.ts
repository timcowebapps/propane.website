"use strict";

/**
 * Перечисление типов фактур.
 */
export enum TextureTypes {
	Matte = 0, /*!< Матовый */
	Satin, /*!< Сатиновый */
	Glossy /*!< Глянцевый */
}
