"use strict";

export interface ICostCanvas {
	montage: number;
	cloth: number;
	pvh: number;
}

export interface ICost {
	canvas: ICostCanvas;
	corner: number;
	pipe: number;
	luster: number;
	lamp: number;
}
