"use strict";

import * as Loadable from "react-loadable";
import AsyncLoader from "./../../partials/asyncloader";

const path = require("path");

const loadableOptions: LoadableExport.OptionsWithoutRender<any> = {
	loader: () => import(/* webpackChunkName: "Home" */"./index"),
	webpack: () => [(require as any).resolveWeak("./index")],
	modules: [path.join(__dirname, "./index")],
	loading: AsyncLoader
}

export default Loadable(loadableOptions);
