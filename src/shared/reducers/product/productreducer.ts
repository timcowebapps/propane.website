"use strict";

import { IAction, ActionTypeStates, Actions } from "./../../sagas/@exports";
import { IProductReducerState } from "./productreducerstate";

export function productReducer(state = {
	loaded: false,
	items: []
} as IProductReducerState, action: IAction<any>) {
	switch (action.type) {

		//#region Получение списка продуктов

		case Actions.ProductListActionTypeState.toStr(ActionTypeStates.REQUEST):
		case Actions.ProductListActionTypeState.toStr(ActionTypeStates.FAILURE):
			return { loaded: false };

		case Actions.ProductListActionTypeState.toStr(ActionTypeStates.SUCCESS):
			return { loaded: true, ...action.payload };

		//#endregion

		default:
			return state;
	}
}
