"use strict";

export interface IReducerStoreState {
	router: any;
	calculator: any;
	product: any;
	cart: any;
}
