"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { Models } from "./../../@imports";

export namespace ReviewListProps {
	export interface IProps extends React.Props<any> {
		list: Models.IReview[];
		page: number;
		onPaginatedSearch: any;
	}

	export const types: PropTypes.ValidationMap<any/*IProps*/> = {
		// Empty
	}

	export const defaults: IProps = {
		list: [],
		page: 0,
		onPaginatedSearch: undefined
	}
}
