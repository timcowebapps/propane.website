"use strict";

import * as Stores from "./../../stores/fakestore";
import * as Models from "./../../stores/models/@export-models";
import * as Partials from "./../partials/@export-partials";

export {
	Stores,
	Models,
	Partials
};
