"use strict";

import * as PropTypes from "prop-types";
import { Data } from "@timcowebapps/react.componentmodel";

export namespace CustomSliderProps {
	export interface IProps extends Data.Schema.IComponentProps {
		// Empty
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		// Empty
	}

	export const defaults: IProps = {
		requirements: {
			name: "unnamed",
			min: 0, max: 50, step: 1,
			value: NaN,
			unit: "",
			onChanged: undefined
		},
		items: [{
			uid: "label",
			requirements: { text: "unlabeled" }
		}, {
			uid: "input",
			requirements: { name: "nameless" }
		}]
	}
}
