"use strict";

import { Dynamic } from "@timcowebapps/react.system";
import { Data } from "@timcowebapps/react.componentmodel";
import { AlignTypes, AlignTransform, HtmlTagTypes } from "@timcowebapps/react.utils";

const headingStyles = require("./variants.scss");

export namespace HeadingVariants {
	export function getHeadTitleSheme(styles: Dynamic.IAnyObject, content) {
		return {
			requirements: {
				htmlTag: HtmlTagTypes.H1,
				align: AlignTransform.toStr(AlignTypes.Left),
				viewStyle: { stylesheet: headingStyles, bem: { block: "heading", modifiers: ["banner"] } }
			},
			items: [{
				type: Data.Schema.ComponentTypes.Node,
				requirements: {
					content: content
				}
			}]
		}
	}

	export function getSectionTitleSheme(styles: Dynamic.IAnyObject, content) {
		return {
			requirements: {
				htmlTag: HtmlTagTypes.H3,
				align: AlignTransform.toStr(AlignTypes.Center),
				viewStyle: { stylesheet: headingStyles, bem: { block: "heading" } }
			},
			items: [{
				type: Data.Schema.ComponentTypes.Node,
				requirements: {
					content: content
				}
			}]
		}
	}

	export function getFootTitleSheme(styles: Dynamic.IAnyObject, content) {
		return {
			requirements: {
				htmlTag: HtmlTagTypes.H4,
				align: AlignTransform.toStr(AlignTypes.Left),
				viewStyle: { stylesheet: headingStyles, bem: { block: "heading" } }
			},
			items: [{
				type: Data.Schema.ComponentTypes.Node,
				requirements: {
					content: content
				}
			}]
		}
	}
}
