"use strict";

import { RouteConstants } from "./../shared/routeconstants";
import Home from "./../shared/components/views/home/index";
import Calc from "./../shared/components/views/calc/index";
import Shop from "./../shared/components/views/shop/index";
import Reviews from "./../shared/components/views/reviews/index";
import NotFound from "./../shared/components/views/notfound/index";

export const Routes = [
	{
		key: "home",
		exact: true,
		path: RouteConstants.Home,
		component: Home
	}, {
		key: "calc",
		exact: true,
		path: RouteConstants.Calc,
		component: Calc
	}, {
		key: "shop",
		exact: true,
		path: RouteConstants.Shop,
		component: Shop
	}, {
		key: "reviews",
		exact: true,
		path: RouteConstants.Reviews,
		component: Reviews
	}, {
		key: "notFound",
		exact: false,
		path: "*",
		component: NotFound
	}
];
