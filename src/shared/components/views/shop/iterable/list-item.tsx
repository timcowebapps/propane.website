"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { connect } from "react-redux";
import { Methodology } from "@timcowebapps/react.style";
import { Data } from "@timcowebapps/react.componentmodel";
import { EnumTransform, HtmlTagTypes, AlignTypes, AlignTransform } from "@timcowebapps/react.utils";
import { Heading, Button, Price } from "@timcowebapps/react.toolkit";
import { Models } from "./../../@imports";
import { ProductListItemProps } from "./list-item-props";
import { ProductListItemState } from "./list-item-state";

const styles = require("./../index.scss");
const headingStyles = require("./../../../ui/typography/heading/variants.scss");

export default connect(ProductListItemProps.mapStateToProps, ProductListItemProps.mapDispatchToProps)(
	class ProductListItem extends React.Component<ProductListItemProps.IProps, ProductListItemState.IState> {
		//#region Статические переменные

		public static displayName: string = "ProductListItem";
		public static propTypes: PropTypes.ValidationMap<any/*ProductListItemProps.IProps*/> = ProductListItemProps.types;
		public static defaultProps: ProductListItemProps.IProps = ProductListItemProps.defaults;

		//#endregion

		//#region Приватные методы

		/**
		 * Начальное состояние свойств по умолчанию.
		 * 
		 * @class ProductListItem
		 * @private
		 */
		private _getInitialState(): ProductListItemState.IState {
			return {
				// Empty
			}
		}

		//#endregion

		/**
		 * Конструктор класса.
		 * 
		 * @class ProductListItem
		 * @public
		 * @constructor
		 * @param {ProductListItemProps.IProps} props Свойства компонента.
		 */
		public constructor(props?: ProductListItemProps.IProps) {
			super(props);

			this.state = this._getInitialState();
		}

		/**
		 * Отрисовывает компонент.
		 * 
		 * @class ProductListItem
		 * @public
		 */
		public render(): JSX.Element {
			const product: Models.IProduct = (this.props as any).requirements.payload;

			return (
				React.createElement("div", {
					className: styles["product-card"],
					itemScope: true,
					itemType: "http://schema.org/Product"
				},
					<React.Fragment>
						<Heading {...{
							requirements: {
								htmlTag: HtmlTagTypes.H6,
								align: AlignTransform.toStr(AlignTypes.Left),
								viewStyle: { stylesheet: headingStyles, bem: { block: "heading" } }
							},
							items: [{
								type: Data.Schema.ComponentTypes.Node,
								requirements: {
									content: <React.Fragment>
										Светильник&nbsp;
										<span itemProp="brand">{product.brand}</span>&nbsp;
										<span itemProp="name">{product.model}</span>
									</React.Fragment>
								}
							}]
						}} />

						<a className={styles["image-link"]} itemProp="image" href={product.thumb}>
							<img src={product.thumb} title="Светильник" />
						</a>

						<table className={styles["tech-features"]}>
							<caption className={Methodology.Bem.Entities.block(styles, "text").element().modifiers(["left"])}>Технические характеристики</caption>
							<tbody>
								<tr>
									<td>Материал</td>
									<td>{product.specs.material}</td>
								</tr>
								<tr>
									<td>Цвет</td>
									<td>{product.specs.color}</td>
								</tr>
								<tr>
									<td>Цоколь</td>
									<td>{product.specs.cap}</td>
								</tr>
								<tr>
									<td>Размеры</td>
									<td>{product.specs.dimensions.diameter}*{product.specs.dimensions.depth}мм/{product.specs.dimensions.cut}мм</td>
								</tr>
								<tr>
									<td>Напряжение</td>
									<td>{product.params.voltage}В/{product.params.frequency}Гц</td>
								</tr>
							</tbody>
						</table>

						{/* Микроданные рейтинга */}
						<span itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
							<meta itemProp="ratingValue" content="4.4" />
							<meta itemProp="ratingCount" content="89" />
						</span>

						<div itemProp="description">Описание продукта.</div>

						{/* Цена продукта / Микроданные */}
						<span itemProp="offers" itemScope itemType="http://schema.org/Offer">
							<meta itemProp="price" content={product.price.sale.toString()} />
							<meta itemProp="priceCurrency" content={EnumTransform.toStr(Models.CurrencyTypes, Models.CurrencyTypes.RUB)} />

							<Price {...{
								requirements: {
									htmlTag: HtmlTagTypes.Del,
									amount: product.price.original, currency: "руб",
									viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["origin"] } }
								}
							}} />

							<Price {...{
								requirements: {
									htmlTag: HtmlTagTypes.Span,
									amount: product.price.sale, currency: "руб",
									viewStyle: { stylesheet: styles, bem: { block: "price", modifiers: ["sale"] } }
								}
							}} />
							<link itemProp="availability" href="http://schema.org/InStock" />
							<div>В наличии</div>
						</span>

						<div>
							<Button {...{
								requirements: {
									htmlTag: HtmlTagTypes.Button,
									onClick: (event: React.MouseEvent<HTMLButtonElement>) => this.props.actionCreators.addToCart(product),
									viewStyle: { stylesheet: styles, bem: { block: "btn", modifiers: ["primary"] }, extracts: "block" }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Node,
									requirements: { content: "Заказать" }
								}]
							}} />
						</div>
					</React.Fragment>
				)
			);
		}
	}
);
