"use strict";

import * as React from "react";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router"
import { HashRouter, Switch } from "react-router-dom";
import { RoutePolicy } from "./../shared/routepolicy";
import { Layout } from "./../shared/components/layout";
import { Routes } from "./routes";

export const Root = ({ store, routerHistory }: { store: any, routerHistory: any }) => (
	<Provider store={store} key={Math.random()}>
		<HashRouter>
			<ConnectedRouter history={routerHistory} key={Math.random()}>
				<Layout>
					<Switch>
						{Routes.map((route) => <RoutePolicy component={route.component} {...route} key={route.key} />)}
					</Switch>
				</Layout>
			</ConnectedRouter>
		</HashRouter>
	</Provider>
);
