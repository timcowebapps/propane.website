"use strict";

import * as _ from "lodash";
import { takeLatest, all, put } from "redux-saga/effects";
import { IProduct } from "./../stores/models/product/product";
import { FirebaseService } from "./../services/firebaseservice";
import { ActionTypeStates, Actions } from "./@exports";

export function* doChangeCalculatorDataRequest(action: any) {
	try {
		yield put(Actions.changeDataSuccess(action.payload.data));
	} catch (error) {
		let message = error;
		yield put(Actions.changeDataFailure(message));
	}
}

export function* doProductListRequest(action: any) {
	try {
		let querySnapshot: firebase.firestore.QuerySnapshot = yield FirebaseService.instance.firestore.collection("products").get();
		let products: Array<IProduct> = yield all(querySnapshot.docs.map((item: any) => _.assign({}, item.data(), { id: item.id })));

		yield put(Actions.productListSuccess(products));
	} catch (error) {
		let message = error;
		yield put(Actions.productListFailure(message));
	}
}

export function* doAddToCartRequest(action: any) {
	try {
		//let querySnapshot = yield FirebaseService.instance.firestore.collection("products").doc(action.payload.productId).get();
		//yield put(Actions.addToCartSuccess(querySnapshot.data()));
		yield put(Actions.addToCartSuccess(action.payload.item));
	} catch (error) {
		let message = error;
		yield put(Actions.addToCartFailure(message));
	}
}

export function* doRemoveFromCartRequest(action: any) {
	try {
		yield put(Actions.removeFromCartSuccess(action.payload.productId));
	} catch (error) {
		let message = error;
		yield put(Actions.removeFromCartFailure(message));
	}
}

export default function* saga() {
	yield takeLatest(Actions.ChangeDataActionTypeState.toStr(ActionTypeStates.REQUEST), doChangeCalculatorDataRequest);
	yield takeLatest(Actions.ProductListActionTypeState.toStr(ActionTypeStates.REQUEST), doProductListRequest);
	yield takeLatest(Actions.AddToCartActionTypeState.toStr(ActionTypeStates.REQUEST), doAddToCartRequest);
	yield takeLatest(Actions.RemoveFromCartActionTypeState.toStr(ActionTypeStates.REQUEST), doRemoveFromCartRequest);
}
