"use strict";

import { Action, ActionCreator } from "redux";
import { ICostServicePayload } from "./../../stores/models/calc/cost-service";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { ChangeDataActionTypeState } from "./changedataactiontypestate";

export const changeDataRequestAction: ActionCreator<Action> = (data: ICostServicePayload): IAction<any> => {
	return {
		type: ChangeDataActionTypeState.toStr(ActionTypeStates.REQUEST),
		payload: {
			data: data
		}
	}
}

export const changeDataRequest = changeDataRequestAction;
