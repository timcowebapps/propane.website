"use strict";

import { Action, ActionCreator } from "redux";
import { IProduct } from "./../../stores/models/product/product";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { ProductListActionTypeState } from "./productlistactiontypestate";

export const productListSuccessAction: ActionCreator<Action> = (products: Array<IProduct>): IAction<any> => {
	return {
		type: ProductListActionTypeState.toStr(ActionTypeStates.SUCCESS),
		payload: {
			items: products
		}
	};
}

export const productListSuccess = productListSuccessAction;
