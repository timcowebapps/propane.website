"use strict";

import * as _ from "lodash";
import * as React from "react";
import * as PropTypes from "prop-types";
import { Data } from "@timcowebapps/react.componentmodel";
import { HtmlTagTypes, PositionTypes } from "@timcowebapps/react.utils";
import { Navbar, Drawer, Button, Switch, Modal } from "@timcowebapps/react.toolkit";
import DrawerMenu from "./../drawer-menu/index";
import { AsideProps } from "./index-props";
import { AsideState } from "./index-state";

const styles: any = require("./index.scss");

export default class Aside extends React.Component<AsideProps.IProps, AsideState.IState> {
	//#region Статические переменные

	public static displayName: string = "Aside";
	public static propTypes: PropTypes.ValidationMap<AsideProps.IProps> = AsideProps.types;
	public static defaultProps: AsideProps.IProps = AsideProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Aside
	 * @private
	 */
	private _getInitialState(): AsideState.IState {
		return {
			opened: false
		}
	}

	private _handleSwitchChange(event: React.SyntheticEvent<HTMLInputElement>, value: boolean): void {
		this.setState({ opened: !this.state.opened });
	}

	private _handleViewCartClick(): void {
		// Empty
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Aside
	 * @public
	 * @constructor
	 * @param {AsideProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: AsideProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		//#region Bindings

		this._handleSwitchChange = this._handleSwitchChange.bind(this);
		this._handleViewCartClick = this._handleViewCartClick.bind(this);

		//#endregion
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Aside
	 * @public
	 */
	public render(): JSX.Element {
		return <Navbar {...{
			requirements: {
				htmlTag: HtmlTagTypes.Aside,
				viewStyle: {
					stylesheet: styles,
					bem: {
						block: "aside",
						//modifiers: ["fixed", "right"]
						modifiers: ["sticky", "right"]
					}
				}
			}
		}}>
			{/* Кнопка-переключатель (показывает/скрывает навигационное меню) */}
			<Switch {...{
				uid: "hamburger",
				defaults: { checked: false },
				requirements: {
					checked: this.state.opened,
					onChange: this._handleSwitchChange,
					viewStyle: { stylesheet: styles, bem: { block: "hamburger", modifiers: ["menulink"] } }
				},
				items: [{
					type: Data.Schema.ComponentTypes.Icon,
					requirements: {
						width: "1em", height: "1em",
						viewBox: { x: 0, y: 0, width: 24, height: 24 },
						preserveAspectRatio: "xMidYMid meet",
					},
					items: [{
						type: Data.Schema.ComponentTypes.Node,
						requirements: {
							content: React.createElement("g", { key: Math.random() },
								<React.Fragment>
									<path key={Math.random()} d="m 3,5  a 1.0001,1.0001 0 1 0 0,2 h 18 a 1.0001,1.0001 0 1 0 0,-2 z" />
									<path key={Math.random()} d="m 3,11 a 1.0001,1.0001 0 1 0 0,2 h 18 a 1.0001,1.0001 0 1 0 0,-2 z" />
									<path key={Math.random()} d="m 3,11 a 1.0001,1.0001 0 1 0 0,2 h 18 a 1.0001,1.0001 0 1 0 0,-2 z" />
									<path key={Math.random()} d="m 3,17 a 1.0001,1.0001 0 1 0 0,2 h 18 a 1.0001,1.0001 0 1 0 0,-2 z" />
								</React.Fragment>
							)
						}
					}]
				}]
			}} />

			<Drawer {...{
				uid: "drawer_id",
				requirements: {
					hidden: !this.state.opened,
					position: PositionTypes.Top,
					debounceWait: 1200,
					viewStyle: { stylesheet: styles, bem: { block: "drawer" } }
				},
				items: [{
					type: Data.Schema.ComponentTypes.Backdrop,
					requirements: {
						// Empty
					}
				}, {
					type: Data.Schema.ComponentTypes.Panel,
					requirements: {
						content: <DrawerMenu onCloseMenu={() => {
							this.setState({ opened: false });
						}} />
					}
				}]
			}} />

			<Button {...{
				requirements: {
					htmlTag: HtmlTagTypes.Button,
					onClick: this._handleViewCartClick,
					viewStyle: { stylesheet: styles, bem: { block: "btn-shopping-cart", modifiers: ["menulink"] }, extracts: "block" }
				},
				items: [{
					type: Data.Schema.ComponentTypes.Icon,
					requirements: {
						width: "1em", height: "1em",
						viewBox: { x: 0, y: 0, width: 24, height: 24 },
						preserveAspectRatio: "xMidYMid meet",
						viewStyle: { stylesheet: styles, bem: { block: "icon" } }
					},
					items: [{
						type: Data.Schema.ComponentTypes.Node,
						requirements: {
							content: React.createElement("g", { key: Math.random() },
								<React.Fragment>
									<path key={Math.random()} d="m 9,19 c -1.0927249,0 -2,0.907275 -2,2 0,1.092725 0.9072751,2 2,2 1.092725,0 2,-0.907275 2,-2 0,-1.092725 -0.907275,-2 -2,-2 z" />
									<path key={Math.random()} d="m 20,19 c -1.092725,0 -2,0.907275 -2,2 0,1.092725 0.907275,2 2,2 1.092725,0 2,-0.907275 2,-2 0,-1.092725 -0.907275,-2 -2,-2 z" />
									<path key={Math.random()} d="m 1,0 a 1.0001,1.0001 0 1 0 0,2 h 3.1796875 l 0.8085937,4.0351562 a 1.0001,1.0001 0 0 0 0.0625,0.3164063 l 1.6484376,8.2343755 c 0.2825144,1.417987 1.5542496,2.441768 3,2.414062 h 9.6816402 c 1.44634,0.02772 2.718178,-0.997127 3,-2.416016 a 1.0001,1.0001 0 0 0 0.002,-0.0059 L 23.982422,6.1875 A 1.0001,1.0001 0 0 0 23,5 H 6.8203125 L 5.9804688,0.8046875 A 1.0001,1.0001 0 0 0 5,0 Z M 7.2207031,7 H 21.791016 l -1.371094,7.195312 c -0.09565,0.481575 -0.509108,0.814095 -1,0.804688 a 1.0001,1.0001 0 0 0 -0.01953,0 H 9.6796875 a 1.0001,1.0001 0 0 0 -0.019531,0 c -0.490892,0.0094 -0.9043486,-0.323113 -1,-0.804688 a 1.0001,1.0001 0 0 0 0,-0.002 z" />
								</React.Fragment>
							)
						}
					}]
				}]
			}} />

		</Navbar>
	}
}
