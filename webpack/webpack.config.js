"use strict";

const path = require("path");
const webpack = require("webpack");
const merge = require("webpack-merge");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const HtmlChangeAssetsExtensionPlugin = require("html-webpack-change-assets-extension-plugin")
const HtmlPlugin = require("html-webpack-plugin");
const ReactLoadablePlugin = require("react-loadable/webpack").ReactLoadablePlugin;
const CopyPlugin = require("copy-webpack-plugin");
const ManifestPlugin = require("webpack-manifest-plugin");
const pkg = require("./../package.json");
const pkgConfig = require("./../package.config.js");

module.exports = merge(require("./webpack.config.base.js"), {
	name: "prod.client",
	mode: "production",
	devtool: "inline-source-map",
	node: {
		fs: "empty"
	},
	entry: {
		"bundle.client": path.resolve(__dirname, "../", pkgConfig.paths.src.base, "client/index.tsx")
	},
	output: {
		filename: pkgConfig.disabledChunkhash
			? "[name].js"
			: "[name].[chunkhash:8].js",
		sourceMapFilename: pkgConfig.disabledChunkhash
			? "[name].map"
			: "[name].[chunkhash:8].map",
		chunkFilename: pkgConfig.disabledChunkhash
			? "[name].chunk.js"
			: "[name].[chunkhash:8].chunk.js",
		publicPath: pkgConfig.urls.publicPath
	},
	optimization: {
		runtimeChunk: {
			name: "runtime",
		},
		chunkIds: "named",
		moduleIds: "hashed",
		splitChunks: {
			hidePathInfo: true,
			chunks: "initial",
			maxInitialRequests: Infinity,
			maxAsyncRequests: Infinity,
			minSize: 10000,
			maxSize: 40000,
			cacheGroups: {
				default: false,
				lodash: {
					test: module => module.context.indexOf("node_modules/lodash") >= 0,
					name: "vendor.test.lodash",
					chunks: "all",
					priority: 20,
					enforce: true
				},
				firebase: {
					test: module => module.context.indexOf("node_modules/firebase") >= 0,
					name: "vendor.test.firebase",
					chunks: "all",
					priority: 19,
					enforce: true
				},
				reactDOM: {
					test: module => module.context.indexOf("node_modules/react-dom") >= 0,
					name: "vendor.test.react-dom",
					chunks: "all",
					priority: 18,
					enforce: true
				},
				reactRedux: {
					test: module => module.context.indexOf("node_modules/react-redux") >= 0,
					name: "vendor.test.react-redux",
					chunks: "all",
					priority: 17,
					enforce: true
				},
				reduxSaga: {
					test: module => module.context.indexOf("node_modules/redux-saga") >= 0,
					name: "vendor.test.redux-saga",
					chunks: "all",
					priority: 16,
					enforce: true
				},
				vendorSplit: {
					test: module => {
						if (!module.context.includes("node_modules"))
							return false;

						if ([
							"lodash", "firebase", "react-dom", "react-redux", "redux-saga"
						].some(str => module.context.includes(str)))
							return false;

						return true;
					},
					name: module => {
						const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
						return `vendor.${packageName.replace("@", "")}`;
					},
					priority: 1
				},
				vendors: {
					test: /[\\/]node_modules[\\/]/,
					name: "vendors",
					priority: 0,
					enforce: true
				},
				// vendorAsyncSplit: {
				// 	test: /[\\/]node_modules[\\/]/,
				// 	name(module) {
				// 		const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
				// 		return `vendor.async.${packageName.replace("@", "")}`;
				// 	},
				// 	chunks: "async",
				// 	priority: 10,
				// 	reuseExistingChunk: true,
				// 	minSize: 5000
				// },
				// vendorsAsync: {
				// 	test: /[\\/]node_modules[\\/]/,
				// 	name: "vendors.async",
				// 	chunks: "async",
				// 	priority: 9,
				// 	reuseExistingChunk: true,
				// 	enforce: true
				// },
				// commonAsync: {
				// 	name(module) {
				// 		const moduleName = module.context.match(/[^\\/]+(?=\/$|$)/)[0];
				// 		return `common.async.${moduleName.replace("@", "")}`;
				// 	},
				// 	minChunks: 2,
				// 	chunks: "async",
				// 	priority: 1,
				// 	reuseExistingChunk: true,
				// 	minSize: 5000
				// },
				// commonsAsync: {
				// 	name: "commons.async",
				// 	minChunks: 2,
				// 	chunks: "async",
				// 	priority: 0,
				// 	reuseExistingChunk: true,
				// 	enforce: true
				// }
			}
		},
		minimizer: [
			new TerserPlugin({
				cache: true,
				parallel: true,
				sourceMap: true
			}),
			new OptimizeCSSAssetsPlugin()
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: pkgConfig.disabledChunkhash
				? "[name].css"
				: "[name].[contenthash:8].css",
			chunkFilename: pkgConfig.disabledChunkhash
				? "[name].chunk.css"
				: "[name].[contenthash:8].chunk.css",
		}),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: JSON.stringify("production"),
				BROWSER: JSON.stringify(true),
				VERSION: JSON.stringify(pkg.version)
			}
		}),
		new webpack.ProvidePlugin({
			"React": "react",
			"ReactDOM": "react-dom"
		}),
		new CopyPlugin([
			{
				from: path.resolve(__dirname, "../assets/"),
				to: path.resolve(__dirname, "../wwwroot/")
			}, {
				from: path.resolve(__dirname, "../src/manifest.json"),
				to: path.resolve(__dirname, "../wwwroot/")
			}, {
				from: path.resolve(__dirname, "../src/robots.txt"),
				to: path.resolve(__dirname, "../wwwroot/")
			}, {
				from: path.resolve(__dirname, "../src/sitemap.xml"),
				to: path.resolve(__dirname, "../wwwroot/")
			}
		]),
		new ManifestPlugin({
			fileName: path.resolve(__dirname, "../wwwroot/asset-manifest.json")
		}),
		new ReactLoadablePlugin({
			filename: path.resolve(__dirname, "../wwwroot/react-loadable.json")
		}),
		new HtmlPlugin({
			title: pkg.name + ' - ' + pkg.description,
			filename: path.resolve(__dirname, "../wwwroot/index.cshtml"),
			template: path.resolve(__dirname, "../", pkgConfig.paths.src.tmpl),
			hash: false,
			cache: true,
			showErrors: false,
			//excludeChunks: ["manifest"],
			inject: true,
			injectComponent: {
				markup: "@html.raw(model.markup)",
				scripts: "@html.raw(model.scripts.join())",
				styles: "@html.raw(model.styles.join())",
				preloadedState: "@html.raw(model.preloadedState)"
			},
			minify: {
				collapseWhitespace: true,
				collapseInlineTagWhitespace: true,
				removeComments: true,
				removeRedundantAttributes: true,
				minifyJS: true,
				minifyCSS: true,
				minifyURLs: true
			}
		}),
		new CompressionPlugin({
			filename: "[path].gz[query]",
			algorithm: "gzip",
			test: /\.js$|\.css$|\.html$/,
			threshold: 10240,
			minRatio: 0.8
		}),
		new HtmlChangeAssetsExtensionPlugin()
	]
});
