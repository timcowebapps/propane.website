"use strict";

const webpack = require("webpack");
const pkgConfig = require("./../package.config.js");
const webpackConfig = require(`./../webpack/webpack.config${pkgConfig.devbuild ? ".dev" : ""}.js`);

var compiler = webpack(webpackConfig);
compiler.run(function (err, stats) {
	if (err)
		return console.error(err.message);

	console.log(stats.toString({
		colors: true
	}));
});
