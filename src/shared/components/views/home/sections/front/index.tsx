"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { Methodology } from "@timcowebapps/react.style";
import { Data } from "@timcowebapps/react.componentmodel";
import { CN, HtmlTagTypes } from "@timcowebapps/react.utils";
import { FrontProps } from "./index-props";
import { FrontState } from "./index-state";
import { Heading, Button } from "@timcowebapps/react.toolkit";
import { HeadingVariants } from "./../../../../ui/typography/heading/variants";

//#region Import Assets

const styles = require("./index.scss");

//#endregion

export default class Front extends React.Component<FrontProps.IProps, FrontState.IState> {
	//#region Статические переменные

	public static displayName: string = "Front";
	public static propTypes: PropTypes.ValidationMap<FrontProps.IProps> = FrontProps.types;
	public static defaultProps: FrontProps.IProps = FrontProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Front
	 * @private
	 */
	private _getInitialState(): FrontState.IState {
		return {
			// Empty
		}
	}

	//#region Events

	private _handleCalcAnchorClick(event): void {
		if (this.props.onSectionChange)
			this.props.onSectionChange(1/*calc section*/);
	}

	//#endregion

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Front
	 * @public
	 * @constructor
	 * @param {FrontProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: FrontProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		//#region Bindings

		this._handleCalcAnchorClick = this._handleCalcAnchorClick.bind(this);

		//#endregion
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Front
	 * @public
	 */
	public render(): JSX.Element {
		return (
			<div className={styles["container"]}>
				<div className={styles["row"]}>
					<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
						<Heading key={2} {...HeadingVariants.getHeadTitleSheme(styles, <React.Fragment key={3}>
							<span key={4} className={Methodology.Bem.Entities.block(styles, "heading").element("line").toStr()}>Натяжные потоки&nbsp;</span>
							<span key={5} className={Methodology.Bem.Entities.block(styles, "heading").element("line").toStr()}>в Санкт-<wbr key={8} />Петербурге и Лен. области</span>
						</React.Fragment>)} />
					</div>
				</div>
				<div className={CN.many(styles["row"], styles["toorder"])}>
					<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-4", "lg-4"])}>
						<div className={styles["service-item"]}>
							<h4>Замер и расчет стоимости</h4>
							<p>Оставьте заявку и наш замерщик приедет к Вам на замер <strong>в течении часа</strong> или в любое <strong>удобное</strong> для Вас время. Расчет стоимости производится <strong>бесплатно</strong> прямо при Вас.</p>
							<Button {...{
								requirements: {
									htmlTag: HtmlTagTypes.Button,
									onClick: (event: any) => { },
									viewStyle: { stylesheet: styles, bem: { block: "btn", modifiers: ["info"] }, extracts: "block" }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Node,
									requirements: { content: "Заказать замер" }
								}]
							}} />
						</div>
					</div>
					<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-4", "lg-4"])}>
						<div className={styles["service-item"]}>
							<h4>Изготовление полотна и каркаса</h4>
							<p>По точным замерам изготавливаем полотно натяжного потолка согласно дизайн-проекту. Укомплектовываем заказ всеми расходными материалами и комплектующими.</p>
							<Button {...{
								requirements: {
									htmlTag: HtmlTagTypes.Button,
									onClick: (event: any) => { },
									viewStyle: { stylesheet: styles, bem: { block: "btn", modifiers: ["info"] }, extracts: "block" }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Node,
									requirements: { content: "Сертификаты качества" }
								}]
							}} />
						</div>
					</div>
					<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-4", "lg-4"])}>
						<div className={styles["service-item"]}>
							<h4>Установка натяжного потолка</h4>
							<p>Бригада приедет <strong>строго в оговоренное время</strong> и установит потолок за несколько часов <strong>без грязи и пыли</strong>. Оплата производится <strong>после монтажа</strong>.</p>
							<Button {...{
								requirements: {
									htmlTag: HtmlTagTypes.Button,
									onClick: (event: any) => { },
									viewStyle: { stylesheet: styles, bem: { block: "btn", modifiers: ["info"] }, extracts: "block" }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Node,
									requirements: { content: "Что входит в гарантию" }
								}]
							}} />
						</div>
					</div>
				</div>
				<div className={styles["row"]}>
					<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
						<Button {...{
							requirements: {
								htmlTag: HtmlTagTypes.Button,
								onClick: this._handleCalcAnchorClick,
								viewStyle: { stylesheet: styles, bem: { block: "btn" } }
							},
							items: [{
								type: Data.Schema.ComponentTypes.Node,
								requirements: {
									content: "Хотите узнать примерную стоимость потока?"
								}
							}]
						}} />
					</div>
				</div>
			</div>
		);
	}
}
