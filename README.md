##PROPANE

[![License][license-svg]][license-url]

###Доступные команды

#####Сборка под Production:

```console
npm run build
```

#####Сборка под Development:

```console
npm run build:dev
```

#####Разные команды:

```console
npm run clean
```
> Удаляет **./wwwroot/** папку с выходными данными

###Heroku команды

```console
heroku run bash --app propane-spb
```
Ctrl-D exit

```console
heroku logs -t --app propane-spb
```

```console
heroku ps:scale web=1 --app propane-spb
heroku restart --app propane-spb
```

[license-svg]: https://img.shields.io/github/license/mashape/apistatus.svg
[license-url]: LICENSE
