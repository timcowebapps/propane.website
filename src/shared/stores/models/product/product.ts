"use strict";

export enum CurrencyTypes {
	RUB,
	USD
}

export interface IProductPrice {
	original: number;
	sale: number;
	currency: CurrencyTypes;
}

export interface IProductDimensions {
	diameter: number;
	depth: number;
	cut: number;
}

export interface ITechnicalSpecifications {
	color: string; /*!< Цвет светильника */
	material: string; /*!< Материал светильника */
	cap: string; /*!< Цоколь лампы */
	dimensions: IProductDimensions;
}

export interface IElectricalPerformanceParameters {
	voltage: string;
	frequency: string;
}

export interface IProduct {
	id: string;
	brand: string; /*!< Бренд */
	model: string; /*!< Модель */
	thumb: string; /*!< Фотография */
	price: IProductPrice; /*!< Цена */
	specs?: ITechnicalSpecifications; /*!< Технические характеристики */
	params?: IElectricalPerformanceParameters; /*!< Параметры электрических характеристик */
}
