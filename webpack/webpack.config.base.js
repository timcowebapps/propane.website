"use strict";

const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const pkgConfig = require("./../package.config.js");
const alias = require("./alias.js");

module.exports = {
	target: "web",
	output: {
		path: path.resolve(__dirname, "../", pkgConfig.paths.dst.base)
	},
	performance: {
		maxEntrypointSize: 512000,
		maxAssetSize: 512000
	},
	module: {
		rules: [
			{
				test: /\.ts[x]?$/,
				loader: "ts-loader"
			}, {
				test: /\.(scss|css)$/,
				resolve: { extensions: [".scss", ".css"] },
				use: [
					pkgConfig.devbuild
						? { loader: "style-loader" }
						: MiniCssExtractPlugin.loader,
					{
						loader: "css-loader",
						options: {
							importLoaders: 2,
							modules: {
								localIdentName: pkgConfig.devbuild
									? "[path][name]__[local]--[hash:base64:5]"
									: "[hash:base64:5]"
							}
						}
					}, {
						loader: "postcss-loader"
					}, {
						loader: "resolve-url-loader"
					}, {
						loader: "sass-loader"
					}
				]
			}, {
				test: /\.(jp[e]?g|png|gif|svg)$/i,
				loader: "file-loader?name=img/[name].[ext]"
			}, {
				test: /\.html$/,
				loader: "file-loader?name=[name].[ext]"
			}, {
				test: /\.ico$/,
				loader: "file-loader?name=[name].[ext]"
			}
		]
	},
	resolve: {
		extensions: [".js", ".jsx", ".ts", ".tsx"],
		alias: alias,
		modules: [
			path.resolve(__dirname, "../node_modules")
		]
	}
};
