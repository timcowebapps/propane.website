"use strict";

import { Models } from "./../../../@imports";

export namespace CalcState {
	export interface IState {
		/**
		 * Площадь помещения
		 * 
		 * @type {number}
		 * @memberof CalcState.IState
		 */
		area: number;
		
		/**
		 * Количество углов
		 * 
		 * @type {number}
		 * @memberof CalcState.IState
		 */
		number_corners: number;

		/**
		 * Количество труб отопления
		 * 
		 * @type {number}
		 * @memberof CalcState.IState
		 */
		number_pipes: number;

		/**
		 * Тип материала
		 * 
		 * @type {number}
		 * @memberof CalcState.IState
		 */
		selected_material: Models.MaterialTypes;

		/**
		 * Тип помещения
		 * 
		 * @type {number}
		 * @memberof CalcState.IState
		 */
		selected_room: string;
	}
}
