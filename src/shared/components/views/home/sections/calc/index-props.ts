"use strict";

import * as PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { IAction } from "./../../../../../actions/action";
import { Actions } from "./../../../../../sagas/@exports";
import { ICostServicePayload } from "./../../../../../stores/models/calc/cost-service";
import { ISectionProps } from "./../section-props";

export namespace CalcProps {
	export interface IOwnProps extends ISectionProps {
		// Empty
	}

	export interface IStateProps {
		calc: any;
	}

	export interface IDispatchProps {
		actionCreators: {
			changeCalcData?: (payload: ICostServicePayload) => IAction<any>;
		}
	}

	export type IProps = IOwnProps & IStateProps & IDispatchProps;

	export const mapStateToProps = (state: any, ownProp?: any): IStateProps => ({
		calc: state.calculator
	});

	export const mapDispatchToProps = (dispatch: any): IDispatchProps => ({
		actionCreators: bindActionCreators({
			changeCalcData: Actions.changeDataRequest
		}, dispatch)
	});

	export const types: PropTypes.ValidationMap<IProps> = {
		isCurrSection: PropTypes.bool.isRequired,
		onSectionChange: PropTypes.func,
		calc: PropTypes.any,
		actionCreators: PropTypes.shape({
			changeCalcData: PropTypes.func
		})
	}

	export const defaults: IProps = {
		isCurrSection: false,
		onSectionChange: undefined,
		calc: [],
		actionCreators: {
			changeCalcData: undefined
		}
	}
}
