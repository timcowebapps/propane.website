"use strict";

import * as React from "react";
import * as ReactDOM from "react-dom";
import * as PropTypes from "prop-types";
import { Data } from "@timcowebapps/react.componentmodel";
import { HtmlTagTypes } from "@timcowebapps/react.utils";
import { Button } from "@timcowebapps/react.toolkit";
import { ReviewListProps } from "./list-props";
import { ReviewListState } from "./list-state";
import ReviewListItem from "./list-item";
import { Models } from "./../../@imports";

const styles = require("./../index.scss");

export default class ReviewList extends React.Component<ReviewListProps.IProps, ReviewListState.IState> {
	//#region Статические переменные

	public static displayName: string = "ReviewList";
	public static propTypes: PropTypes.ValidationMap<any/*ReviewListProps.IProps*/> = ReviewListProps.types;
	public static defaultProps: ReviewListProps.IProps = ReviewListProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class ReviewList
	 * @private
	 */
	private _getInitialState(): ReviewListState.IState {
		return {
			// Empty
		}
	}

	private _handleLoadMoreClick(event: React.SyntheticEvent<HTMLInputElement>): void {
		this.props.onPaginatedSearch();
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class ReviewList
	 * @public
	 * @constructor
	 * @param {ReviewListProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: ReviewListProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._handleLoadMoreClick = this._handleLoadMoreClick.bind(this);
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class ReviewList
	 * @public
	 */
	public render(): JSX.Element {
		return (
			<React.Fragment>
				{this.props.list.map((item: Models.IReview) => <ReviewListItem key={item.id} entity={item} />)}
				{this.props.page !== null && <Button {...{
					requirements: {
						htmlTag: HtmlTagTypes.Button,
						onClick: this._handleLoadMoreClick,
						viewStyle: { stylesheet: styles, bem: { block: "btn", modifiers: ["default"] }, extracts: "block" }
					},
					items: [{
						type: Data.Schema.ComponentTypes.Node,
						requirements: { content: "Показать ещё" }
					}]
				}} />}
			</React.Fragment>
		);
	}
}
