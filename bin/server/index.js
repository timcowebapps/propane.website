"use strict";

const Loadable = require("react-loadable");
const app = require("./../../wwwroot/dst/bundle.server.js").default();
const pkgConfig = require("./../../package.config.js");

Loadable.preloadAll().then(() => {
	app.listen(pkgConfig.realport, (err) => {
		if (err)
			return console.error(err);

		console.log("Express server listening on port %d, in %s mode",
			pkgConfig.realport, app.get("env"));
	});
});
