"use strict";

import { EnumTransform } from "@timcowebapps/react.utils";
import { SERVICE_CALC } from "./constants";
import { ActionTypes } from "./actiontypes";

//#region Калькулятор

export const addRoom = (uid: string) => (dispatch) => {
	return dispatch({
		type: SERVICE_CALC + EnumTransform.toStr(ActionTypes, ActionTypes.ADD),
		payload: uid
	});
}

export const removeRoom = (uid: string) => (dispatch) => {
	return dispatch({
		type: SERVICE_CALC + EnumTransform.toStr(ActionTypes, ActionTypes.REMOVE),
		payload: uid
	});
}

//#endregion
