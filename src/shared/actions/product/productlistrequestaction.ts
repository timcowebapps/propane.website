"use strict";

import { Action, ActionCreator } from "redux";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { ProductListActionTypeState } from "./productlistactiontypestate";

export const productListRequestAction: ActionCreator<Action> = (): IAction<any> => {
	return {
		type: ProductListActionTypeState.toStr(ActionTypeStates.REQUEST),
		payload: {
			// Empty
		}
	}
}

export const productListRequest = productListRequestAction;
