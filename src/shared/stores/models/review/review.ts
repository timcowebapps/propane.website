"use strict";

export interface IReview {
	id: string;
	date: string;
	contract: string;
	name: string;
	city: string;
	advantages: string;
	disadvantages: string;
	text: string;
}
