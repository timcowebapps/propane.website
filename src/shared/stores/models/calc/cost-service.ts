"use strict";

import { Guid } from "@timcowebapps/react.system";
import { RoomTypes } from "./../case/room-types";
import { MaterialTypes } from "./../case/material-types";

export interface ICostServicePayload {
	selected_room: RoomTypes;
	selected_material: MaterialTypes;
	/** Площадь помещения */
	area: number;
	/** Количество углов */
	number_corners: number;
	/** Количество труб, уходящих в потолок */
	number_pipes: number;
	/** Количество люстр */
	number_lusters: number;
	/** Количество светильников */
	number_lamps: number;
}

export interface ICostServiceTab {
	uid: string;
	payload: ICostServicePayload;
}

export namespace CostServiceTabset {
	export const defaults = [{
		uid: Guid.newGuid(),
		payload: {
			selected_room: RoomTypes.Undefined,
			selected_material: MaterialTypes.PVH,
			area: 12,
			number_corners: 4,
			number_pipes: 0,
			number_lusters: 0,
			number_lamps: 0
		} as ICostServicePayload
	}] as Array<ICostServiceTab>;
}

export interface ICartPersistence {
	save: Function;
	update: Function;
	delete: Function;
	retrieve: Function;
}

export interface IShippingCost {
	calculateCost: Function;
}

export class ShoppingCart {
	// Empty
}
