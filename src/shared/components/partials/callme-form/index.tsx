"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { Methodology } from "@timcowebapps/react.style";
import { Data } from "@timcowebapps/react.componentmodel";
import { AlignTransform, AlignTypes, HtmlTagTypes, HtmlInputTypes } from "@timcowebapps/react.utils";
import { Button, FormGroup, PhoneInput } from "@timcowebapps/react.toolkit";
import { Heading } from "@timcowebapps/react.toolkit";
import { CallmeFormProps } from "./index-props";
import { CallmeFormState } from "./index-state";

const styles = require("./index.scss");
const inputStyles = require("./../../ui/input/variants.scss");

export default class CallmeForm extends React.Component<CallmeFormProps.IProps, CallmeFormState.IState> {
	//#region Статические переменные

	public static displayName: string = "CallmeForm";
	public static propTypes: PropTypes.ValidationMap<CallmeFormProps.IProps> = CallmeFormProps.types;
	public static defaultProps: CallmeFormProps.IProps = CallmeFormProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class CallmeForm
	 * @private
	 */
	private _getInitialState(): CallmeFormState.IState {
		return {
			username: "",
			phone: ""
		}
	}

	//#region Events

	private _handleInputChange(event: React.FormEvent<HTMLInputElement>): void {
		this.setState({ [event.currentTarget.name]: event.currentTarget.value } as any);
	}

	private _handleFormSubmit(event): void {
		event.preventDefault();

		const { username, phone } = this.state;

		// fetch("/api/form", {
		// 	method: "POST",
		// 	headers: {
		// 		"Accept": "application/json",
		// 		"Content-Type": "application/json",
		// 	},
		// 	body: JSON.stringify({
		// 		username: username,
		// 		phone: phone,
		// 	})
		// }).then(function (response) {
		// 	return response.json()
		// }).then(function (body) {
		// 	console.log(body);
		// });

		this.setState({
			username: "",
			phone: ""
		});
	}

	//#endregion

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class CallmeForm
	 * @public
	 * @constructor
	 * @param {CallmeFormProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: CallmeFormProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._handleInputChange = this._handleInputChange.bind(this);
		this._handleFormSubmit = this._handleFormSubmit.bind(this);
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class CallmeForm
	 * @public
	 */
	public render(): JSX.Element {
		return (
			<div className={styles["row"]}>
				<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-8", "md-6", "lg-6"])}>
					<Heading {...{
						requirements: {
							htmlTag: HtmlTagTypes.H2,
							align: AlignTransform.toStr(AlignTypes.Left),
							viewStyle: { stylesheet: styles, bem: { block: "heading" } }
						},
						items: [{
							type: Data.Schema.ComponentTypes.Node,
							requirements: {
								content: "Бесплатный вызов замерщика"
							}
						}]
					}} />

					<Heading {...{
						requirements: {
							htmlTag: HtmlTagTypes.H3,
							align: AlignTransform.toStr(AlignTypes.Left),
							viewStyle: { stylesheet: styles, bem: { block: "heading" } }
						},
						items: [{
							type: Data.Schema.ComponentTypes.Node,
							requirements: {
								content: "+7 (952) 666-52-22"
							}
						}]
					}} />
				</div>
				<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-4", "md-6", "lg-6"])}>
					<form className={styles["callme-form"]} onSubmit={this._handleFormSubmit}>
						<div className={styles["fieldset"]}>
							<FormGroup {...{
								type: Data.Schema.ComponentTypes.FormGroup,
								requirements: {
									viewStyle: { stylesheet: styles, bem: { block: "form-group" } }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Input,
									requirements: {
										htmlTag: HtmlTagTypes.Input,
										htmlType: HtmlInputTypes.Text,
										name: "name",
										placeholder: "Введите ваше имя",
										value: this.state.username,
										validations: [
											Data.Annotations.Validations.MinLength
										],
										onValueChange: this._handleInputChange,
										viewStyle: { stylesheet: styles, bem: { block: "input" } }
									}
								}]
							}} />

							<PhoneInput {...{
								requirements: {
									viewStyle: { stylesheet: styles, bem: { block: "phone-input" } }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Input,
									requirements: {
										htmlType: HtmlInputTypes.Text,
										name: "phone",
										placeholder: "Введите ваш телефон",
										value: this.state.phone,
										validations: [Data.Annotations.Validations.Required],
										onValueChange: this._handleInputChange,
										viewStyle: { stylesheet: inputStyles, bem: { block: "phone-input", element: "input" } }
									}
								}]
							}} />

							<Button {...{
								requirements: {
									htmlTag: HtmlTagTypes.Button,
									type: "submit",
									onClick: (event: any) => { },
									viewStyle: { stylesheet: styles, bem: { block: "btn", modifiers: ["primary"] } }
								},
								items: [{
									type: Data.Schema.ComponentTypes.Node,
									requirements: { content: "Перезвонить мне" }
								}]
							}} />
						</div>
					</form>
				</div>
			</div>
		);
	}
}
