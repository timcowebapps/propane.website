"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";

export namespace ReviewsPageProps {
	export interface IProps extends React.Props<any> {
		// Empty
	}

	export const types: PropTypes.ValidationMap<any/*IProps*/> = {
		// Empty
	}

	export const defaults: IProps = {
		// Empty
	}
}
