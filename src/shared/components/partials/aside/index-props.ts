"use strict";

import * as PropTypes from "prop-types";

export namespace AsideProps {
	export interface IProps extends React.Props<any> {
		// Empty
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		// Empty
	}

	export const defaults: IProps = {
		// Empty
	}
}
