"use strict";

import * as _ from "lodash";
import * as React from "react";
import { IViewStyle, Methodology } from "@timcowebapps/react.style";
import { EmptyCartProps } from "./empty-props";

const EmptyCart: React.StatelessComponent<EmptyCartProps.IProps> = (props: EmptyCartProps.IProps) => {
	const { requirements } = _.merge({}, this.default.defaultProps, props);
	const { stylesheet, bem } = requirements.viewStyle as IViewStyle;

	return React.createElement("div", {
		className: Methodology.Bem.Entities.block(stylesheet, bem.block).element(bem.element).toStr()
	}, "Ваша корзина пуста");
};

EmptyCart.propTypes = EmptyCartProps.types;
EmptyCart.defaultProps = EmptyCartProps.defaults;

export default EmptyCart;
