"use strict";

import { Action, ActionCreator } from "redux";
import { IAction } from "./../action";
import { ActionTypeStates } from "./../actiontypestates";
import { ChangeDataActionTypeState } from "./changedataactiontypestate";

export const changeDataFailureAction: ActionCreator<Action> = (error: any): IAction<any> => {
	return {
		type: ChangeDataActionTypeState.toStr(ActionTypeStates.FAILURE),
		payload: {
			error: error
		}
	};
}

export const changeDataFailure = changeDataFailureAction;
