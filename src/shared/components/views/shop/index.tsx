"use strict";

import * as React from "react";
import * as PropTypes from "prop-types";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Methodology } from "@timcowebapps/react.style";
import { CN } from "@timcowebapps/react.utils";
import { Data } from "@timcowebapps/react.componentmodel";
import { Hyperlink } from "@timcowebapps/react.toolkit";
import { RouteConstants } from "./../../../routeconstants";
import { Partials, Models } from "./../@imports";
import { ShopPageProps } from "./index-props";
import { ShopPageState } from "./index-state";
import ProductListItem from "./iterable/list-item";

const styles = require("./index.scss");
const linkStyles = require("./../../ui/link/variants.scss");

class ShopPage extends React.Component<ShopPageProps.IProps, ShopPageState.IState> {
	//#region Статические переменные

	public static displayName: string = "ShopPage";
	public static propTypes: PropTypes.ValidationMap<any/*ShopPageProps.IProps*/> = ShopPageProps.types;
	public static defaultProps: ShopPageProps.IProps = ShopPageProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class ShopPage
	 * @private
	 */
	private _getInitialState(): ShopPageState.IState {
		return {
			// Empty
		}
	}

	//#region Events

	private _handleHyperlinkClick(event: React.MouseEvent<HTMLElement>): void {
		event.preventDefault();
		this.props.history.replace("/calc");
	}

	//#endregion

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class ShopPage
	 * @public
	 * @constructor
	 * @param {ShopPageProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: ShopPageProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		//#region Bindings

		this._handleHyperlinkClick = this._handleHyperlinkClick.bind(this);

		//#endregion
	}

	/**
	 * Компонент будет примонтирован.
	 * В данный момент у нас нет возможности посмотреть DOM элементы.
	 * 
	 * @class ShopPage
	 * @public
	 */
	public componentWillMount(): void {
		this.props.actionCreators.receiveProducts();
	}

	/**
	 * Компонент примонтировался.
	 * В данный момент у нас есть возможность использовать refs.
	 * 
	 * @class ShopPage
	 * @public
	 */
	public componentDidMount(): void {
		// Empty
	}

	/**
	 * Вызывается сразу перед тем, как компонент будет удален из DOM.
	 * 
	 * @class ShopPage
	 * @public
	 */
	public componentWillUnmount(): void {
		// Empty
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class ShopPage
	 * @public
	 */
	public render(): JSX.Element {
		const { products } = this.props;

		return (
			<div className={styles["container"]}>
				{ /* Список освещения */}
				<div className={CN.many(
					Methodology.Bem.Entities.block(styles, "row").element().modifiers(["ex"]),
					Methodology.Bem.Entities.block(styles, "spacing-above").element().modifiers(["lg"]),
					Methodology.Bem.Entities.block(styles, "spacing-below").element().modifiers(["sm"])
				)}>
					{products.map((product: Models.IProduct, idx: number) => {
						return <div key={idx} className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-6", "md-4", "lg-3"])}>
							<ProductListItem product={product} {...{
								requirements: {
									payload: product
								}
							}} />
						</div>
					})}
				</div>
				<div className={CN.many(
					styles["row"],
					Methodology.Bem.Entities.block(styles, "spacing-below").element().modifiers(["lg"])
				)}>
					<div className={Methodology.Bem.Entities.block(styles, "col").element().modifiers(["xs-12", "sm-12", "md-12", "lg-12"])}>
						<Hyperlink {...{
							requirements: {
								viewStyle: { stylesheet: styles, bem: { block: "link" } },
								to:	RouteConstants.Home,
								onClick: this._handleHyperlinkClick
							},
							items: [{
								type: Data.Schema.ComponentTypes.Node,
								requirements: {
									content: "Вернуться на главную страницу"
								}
							}]
						}} />
					</div>
				</div>
			</div>
		);
	}
}

// export default withRouter(connect(
// 	ShopPageProps.mapStateToProps, ShopPageProps.mapDispatchToProps
// )(ShopPage));

export default connect(
	ShopPageProps.mapStateToProps, ShopPageProps.mapDispatchToProps
)(ShopPage);
