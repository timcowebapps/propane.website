"use strict";

export namespace CartState {
	export interface IState {
		quantity: number;
	}
}
