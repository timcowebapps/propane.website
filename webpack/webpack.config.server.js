"use strict";

const webpack = require("webpack");
const merge = require("webpack-merge");
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const pkg = require("./../package.json");
const pkgConfig = require("./../package.config.js");

module.exports = merge(require("./webpack.config.server.base.js"), {
	name: "prod.server",
	mode: "production",
	devtool: "inline-source-map",
	output: {
		filename: "[name].js",
		publicPath: pkgConfig.urls.publicPath
	},
	optimization: {
		minimizer: [
			new TerserPlugin({
				cache: true,
				parallel: true,
				sourceMap: true
			})
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: pkgConfig.disabledChunkhash
				? "[name].css"
				: "[name].[contenthash:8].css",
			chunkFilename: pkgConfig.disabledChunkhash
				? "[name].chunk.css"
				: "[name].[contenthash:8].chunk.css"
		}),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: JSON.stringify("production"),
				BROWSER: JSON.stringify(false),
				VERSION: JSON.stringify(pkg.version)
			}
		}),
		new webpack.ProvidePlugin({
			"React": "react",
			"ReactDOM": "react-dom"
		})
	]
});
